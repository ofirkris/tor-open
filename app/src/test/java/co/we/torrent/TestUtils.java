package co.we.torrent;


import org.junit.Test;

import co.we.torrent.other.FileIOUtils;

import static org.junit.Assert.assertEquals;

/**
 * Created 02.02.2018 by
 *
 * @author Alexander Artemov
 */
public class TestUtils {

    @Test
    public void checkSize() {
        long torrent = 2110308352l;
        long free = 2302873600l;

        System.out.println(FileIOUtils.bytesToMb(torrent) + "mb of " + FileIOUtils.bytesToMb(free) + "mb");
        assertEquals(1, 1);
    }
}
