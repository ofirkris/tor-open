package co.we.torrent;

import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;

import co.we.torrent.di.app.AppComponent;
import co.we.torrent.di.app.AppContextModule;
import co.we.torrent.di.app.DaggerAppComponent;
import co.we.torrent.di.app.MainComponent;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class AndroidApplication extends MultiDexApplication {
    @NonNull
    private static AppComponent appComponent;
    private static MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initializeDebug();
        initializeDI();
    }

    private void initializeDI() {
        appComponent = DaggerAppComponent.builder()
                .appContextModule(new AppContextModule(this))
                .build();
    }

    private void initializeDebug() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Stetho.initializeWithDefaults(this);
        }
    }

    @NonNull
    public static AppComponent applicationComponent() {
        return appComponent;
    }

    public static MainComponent mainComponent() {
        if (mainComponent == null) {
            mainComponent = appComponent.plusMainComponent();
        }
        return mainComponent;
    }

    public static void clearMainComponent() {
        mainComponent = null;
    }
}