package co.we.torrent.data.core;

public interface FetchMagnetCallback {
    void onMagnetFetched(String hash, TorrentMetaInfo info);
}
