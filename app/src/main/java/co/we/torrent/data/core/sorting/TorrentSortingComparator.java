package co.we.torrent.data.core.sorting;

import java.util.Comparator;

import co.we.torrent.data.core.stateparcel.TorrentStateParcel;

public class TorrentSortingComparator implements Comparator<TorrentStateParcel> {
    private TorrentSorting sorting;

    public TorrentSortingComparator(TorrentSorting sorting) {
        this.sorting = sorting;
    }

    @Override
    public int compare(TorrentStateParcel state1, TorrentStateParcel state2) {
        return TorrentSorting.SortingColumns.fromValue(sorting.getColumnName())
                .compare(state1, state2, sorting.getDirection());
    }
}
