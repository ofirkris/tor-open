package co.we.torrent.data.core.exceptions;

public class FileAlreadyExistsException extends Exception
{
    public FileAlreadyExistsException()
    {

    }

    public FileAlreadyExistsException(String message)
    {
        super(message);
    }
}
