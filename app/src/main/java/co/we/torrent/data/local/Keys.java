package co.we.torrent.data.local;

/**
 * @author Alexander Artemov
 */
public class Keys {
    public static final String MAIN_OPENED_FIRST = "main_opened_first";
    public static final String UPDATE_1_0_6 = "update 106 first";
    public static final String UPDATE_1_0_7 = "update 107 first";
}
