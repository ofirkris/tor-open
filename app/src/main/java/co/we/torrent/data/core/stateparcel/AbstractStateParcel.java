package co.we.torrent.data.core.stateparcel;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.UUID;

/*
 * The class provides an abstract package model, sent from the service.
 */

public abstract class AbstractStateParcel<F extends AbstractStateParcel>
        implements
        Parcelable, Comparable<F>
{
    public String parcelId;

    protected AbstractStateParcel()
    {
        parcelId = UUID.randomUUID().toString();
    }

    protected AbstractStateParcel(String parcelId)
    {
        this.parcelId = parcelId;
    }

    protected AbstractStateParcel(Parcel source)
    {
        parcelId = source.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(parcelId);
    }

    @Override
    public abstract int hashCode();

    @Override
    public abstract boolean equals(Object o);
}
