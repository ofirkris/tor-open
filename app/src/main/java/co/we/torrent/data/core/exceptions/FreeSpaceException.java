package co.we.torrent.data.core.exceptions;

/*
 * Not enough free space exception.
 */

public class FreeSpaceException extends Exception {

    public final long torrentSize;
    public final long totalSize;
    public final boolean hasInfo;

    public FreeSpaceException() {
        this.torrentSize = -1;
        this.totalSize = -1;
        this.hasInfo = false;
    }

    public FreeSpaceException(String message) {
        super(message);
        this.torrentSize = -1;
        this.totalSize = -1;
        this.hasInfo = false;
    }

    public FreeSpaceException(String message, long torrentSize, long totalSize) {
        super(message);
        this.torrentSize = torrentSize;
        this.totalSize = totalSize;
        this.hasInfo = true;
    }
}
