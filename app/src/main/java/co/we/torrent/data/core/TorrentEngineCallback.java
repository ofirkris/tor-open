package co.we.torrent.data.core;

public interface TorrentEngineCallback {
    void onTorrentAdded(String id);

    void onTorrentStateChanged(String id);

    void onTorrentFinished(String id);

    void onTorrentRemoved(String id);

    void onTorrentPaused(String id);

    void onTorrentResumed(String id);

    void onEngineStarted();

    void onTorrentMoved(String id, boolean success);

    void onIpFilterParsed(boolean success);

    void onMagnetLoaded(String hash, byte[] bencode);

    void onTorrentMetadataLoaded(String id, Exception err);

    void onRestoreSessionError(String id);
}
