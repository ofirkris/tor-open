package co.we.torrent.data.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import co.we.torrent.data.services.TorrentTaskService;

/*
 * The receiver for Wi-Fi connection state changes.
 */

public class WifiReceiver extends BroadcastReceiver {
    public static final String ACTION_WIFI_ENABLED = "WifiReceiver.ACTION_WIFI_ENABLED";
    public static final String ACTION_WIFI_DISABLED = "WifiReceiver.ACTION_WIFI_DISABLED";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals((WifiManager.WIFI_STATE_CHANGED_ACTION))) {
            Context appContext = context.getApplicationContext();
            ConnectivityManager manager = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (manager != null) {
                NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                Intent serviceIntent = new Intent(appContext, TorrentTaskService.class);
                serviceIntent.setAction(wifi.isConnected() ? ACTION_WIFI_ENABLED : ACTION_WIFI_DISABLED);
                context.startService(serviceIntent);
            }
        }
    }

    public static IntentFilter getFilter() {
        return new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
    }
}
