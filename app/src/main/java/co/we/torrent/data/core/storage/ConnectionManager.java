package co.we.torrent.data.core.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/*
 * The class provides concurrent access to torrents storage.
 */

class ConnectionManager {
    private final static ConnectionManager INSTANCE = new ConnectionManager();
    private DatabaseHelper helper;

    private ConnectionManager() {
        /* Nothing */
    }

    static synchronized SQLiteDatabase getDatabase(Context context) {
        if (INSTANCE.helper == null) {
            INSTANCE.helper = new DatabaseHelper(context);
        }

        return INSTANCE.helper.getWritableDatabase();
    }
}
