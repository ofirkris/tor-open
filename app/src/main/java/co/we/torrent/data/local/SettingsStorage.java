package co.we.torrent.data.local;


import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import co.we.torrent.R;
import co.we.torrent.presentation.settings.SettingsManager;

/**
 * Created 21.11.2017 by
 *
 * @author Alexander Artemov
 */
@Singleton
public class SettingsStorage {

    private final String WIFI_KEY;
    private final Context context;
    private final KeyStorage storage;
    private final SettingsManager settingsManager;

    @Inject
    public SettingsStorage(KeyStorage storage, Context context) {
        this.storage = storage;
        this.context = context;
        this.settingsManager = new SettingsManager(context);
        this.WIFI_KEY = context.getString(R.string.pref_key_wifi_only);

        if (getUpdateOpenedFirst(Keys.UPDATE_1_0_6)) {
            updateCpuAwake();
            setUpdateOpenedFirst(Keys.UPDATE_1_0_6, false);
        }

        if (getUpdateOpenedFirst(Keys.UPDATE_1_0_7)) {
            updateSaveTorrents();
            setUpdateOpenedFirst(Keys.UPDATE_1_0_7, false);
        }
    }

    private void updateSaveTorrents() {
        final SettingsManager pref = new SettingsManager(context);
        String key = context.getString(R.string.pref_key_save_torrent_files);
        pref.put(key, SettingsManager.Default.saveTorrentFiles);
    }

    public boolean getUpdateOpenedFirst(String version) {
        return storage.getBoolean(version);
    }

    public void setUpdateOpenedFirst(String version, boolean value) {
        storage.saveBoolean(version, value);
    }

    public boolean getMainOpenedFirst() {
        return storage.getBoolean(Keys.MAIN_OPENED_FIRST);
    }

    public void setMainOpenedFirst(boolean value) {
        storage.saveBoolean(Keys.MAIN_OPENED_FIRST, value);
    }

    public void updateWifiOnly(boolean value) {
        settingsManager.put(WIFI_KEY, value);
    }

    public void updateCpuAwake() {
        final SettingsManager pref = new SettingsManager(context);
        String keyCpuSleep = context.getString(R.string.pref_key_cpu_do_not_sleep);
        pref.put(keyCpuSleep, SettingsManager.Default.cpuDoNotSleep);
    }
}
