package co.we.torrent.data.core;

/*
 * The different overall states a torrent can be in.
 */

public enum TorrentStateCode {
    UNKNOWN(-1),
    ERROR(0),
    /*
     * In this state the torrent has finished downloading and
     * is a pure seeder.
     */
    SEEDING(1),
    /*
     * The torrent is being downloaded. This is the state
     * most torrents will be in most of the time. The progress
     * meter will tell how much of the files that has been
     * downloaded.
     */
    DOWNLOADING(2),
    PAUSED(3),
    STOPPED(4),
    /*
     * The torrent has not started its download yet, and is
     * currently checking existing files.
     */
    CHECKING(5),
    /*
     * The torrent is trying to download metadata from peers.
     * This assumes the metadata_transfer extension is in use.
     */
    DOWNLOADING_METADATA(6),
    /*
     * In this state the torrent has finished downloading but
     * still doesn't have the entire torrent. i.e. some pieces
     * are filtered and won't get downloaded.
     */
    FINISHED(7),
    /*
     * If the torrent was started in full allocation mode, this
     * indicates that the (disk) storage for the torrent is
     * allocated.
     */
    ALLOCATING(8),
    /*
     * CHECKING
     * DOWNLOADING_METADATA
     * DOWNLOADING
     * PAUSED and progress <100
     */
    QUEUED_GROUP(9),
    /*
     * SEEDING
     * FINISHED
     * STOPPED
     * PAUSED and progress == 100
     */
    FINISHED_GROUP(10),
    ALL_GROUP(11);

    private final int value;

    TorrentStateCode(int value) {
        this.value = value;
    }

    public static TorrentStateCode fromValue(int value) {
        for (TorrentStateCode ev : TorrentStateCode.class.getEnumConstants()) {
            if (ev.value() == value) {
                return ev;
            }
        }

        return UNKNOWN;
    }

    public int value() {
        return value;
    }
}
