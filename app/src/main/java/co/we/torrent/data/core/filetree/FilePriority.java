package co.we.torrent.data.core.filetree;

import com.frostwire.jlibtorrent.Priority;

import java.io.Serializable;

public class FilePriority implements Serializable {
    private int priority;
    private Type type;

    public enum Type {MIXED, IGNORE, NORMAL, HIGH}

    public FilePriority(Priority priority) {
        this.priority = priority.swig();
        this.type = typeFrom(priority);
    }

    public FilePriority(Type type) {
        this.priority = priorityFrom(type);
        this.type = type;
    }

    public Priority getPriority() {
        return Priority.fromSwig(priority);
    }

    public Type getType() {
        return type;
    }

    public static Type typeFrom(Priority priority) {
        switch (priority) {
            case IGNORE:
                return Type.IGNORE;
            case NORMAL:
            case TWO:
            case THREE:
            case FOUR:
            case FIVE:
            case SIX:
                return Type.NORMAL;
            case SEVEN:
                return Type.HIGH;
            default:
                return null;
        }
    }

    private static int priorityFrom(Type type) {
        switch (type) {
            case IGNORE:
                return Priority.IGNORE.swig();
            case NORMAL:
                return Priority.NORMAL.swig();
            case HIGH:
                return Priority.SEVEN.swig();
            default:
                return -1;
        }
    }
}