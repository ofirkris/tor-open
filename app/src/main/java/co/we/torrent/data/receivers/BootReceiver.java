package co.we.torrent.data.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import co.we.torrent.R;
import co.we.torrent.data.services.TorrentTaskService;
import co.we.torrent.presentation.settings.SettingsManager;

/*
 * The receiver for autostart service.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            SettingsManager pref = new SettingsManager(context.getApplicationContext());
            if (pref.getBoolean(context.getString(R.string.pref_key_autostart), SettingsManager.Default.autostart) &&
                    pref.getBoolean(context.getString(R.string.pref_key_keep_alive), SettingsManager.Default.keepAlive)) {
                Intent i = new Intent(context, TorrentTaskService.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(i);

                } else {
                    context.startService(i);
                }
            }
        }
    }
}
