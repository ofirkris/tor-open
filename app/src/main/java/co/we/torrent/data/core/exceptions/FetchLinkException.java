package co.we.torrent.data.core.exceptions;

/**
 * Torrent file or magnet fetch exception.
 */

public class FetchLinkException extends Exception
{
    public FetchLinkException()
    {

    }

    public FetchLinkException(String message)
    {
        super(message);
    }

    public FetchLinkException(Exception e)
    {
        super(e.getMessage());
        super.setStackTrace(e.getStackTrace());
    }
}
