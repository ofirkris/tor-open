package co.we.torrent.data.core;

import android.os.Bundle;

import co.we.torrent.data.core.stateparcel.TorrentStateParcel;

public interface TorrentServiceCallback {
    void onTorrentStateChanged(TorrentStateParcel state);

    void onTorrentsStateChanged(Bundle states);

    void onTorrentAdded(TorrentStateParcel state);

    void onTorrentRemoved(TorrentStateParcel state);
}
