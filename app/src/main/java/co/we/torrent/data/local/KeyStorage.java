package co.we.torrent.data.local;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * @author Alexander Artemov
 */
@Singleton
public class KeyStorage {

    public final static String DEFAULT_STRING = "";
    public final static int DEFAULT_INT = 0;
    public final static int DEFAULT_LONG = -1;
    private static final boolean DEFAULT_BOOL = true;

    @NonNull
    private SharedPreferences preferences;

    @Inject
    public KeyStorage(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    void saveBoolean(String key, boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key) {
        return preferences.getBoolean(key, DEFAULT_BOOL);
    }

    public int getInt(String key) {
        return preferences.getInt(key, DEFAULT_INT);
    }

    public void saveInt(String key, int id) {
        preferences.edit().putInt(key, id).apply();
    }

    public long getLong(String key) {
        return preferences.getLong(key, DEFAULT_LONG);
    }

    public void saveLong(String key, long id) {
        preferences.edit().putLong(key, id).apply();
    }
}
