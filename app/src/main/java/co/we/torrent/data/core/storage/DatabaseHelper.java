package co.we.torrent.data.core.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * A database model for store torrents.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "torrentclient.db";
    private static final int DATABASE_VERSION = 1;

    static final String TORRENTS_TABLE = "torrents";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_TORRENT_ID = "torrent_id";
    static final String COLUMN_NAME = "name";
    static final String COLUMN_PATH_TO_TORRENT = "path_to_torrent";
    static final String COLUMN_PATH_TO_DOWNLOAD = "path_to_download";
    static final String COLUMN_FILE_PRIORITIES = "file_priorities";
    static final String COLUMN_IS_SEQUENTIAL = "is_sequential";
    static final String COLUMN_IS_FINISHED = "is_finished";
    static final String COLUMN_IS_PAUSED = "is_paused";
    static final String COLUMN_DOWNLOADING_METADATA = "downloading_metadata";
    static final String COLUMN_DATETIME = "datetime";

    private static final String CREATE_TORRENTS_TABLE = "create table if not exists "
            + TORRENTS_TABLE +
            "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_TORRENT_ID + " text not null unique, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_PATH_TO_TORRENT + " text not null, "
            + COLUMN_PATH_TO_DOWNLOAD + " text not null, "
            + COLUMN_FILE_PRIORITIES + " text not null, "
            + COLUMN_IS_SEQUENTIAL + " integer, "
            + COLUMN_IS_FINISHED + " integer, "
            + COLUMN_IS_PAUSED + " integer, "
            + COLUMN_DOWNLOADING_METADATA + " integer, "
            + COLUMN_DATETIME + " integer );";

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TORRENTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}
