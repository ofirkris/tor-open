package co.we.torrent.data.core.exceptions;

/**
 * Torrent file or magnet fetch exception.
 */

public class WifiOnlyException extends Exception {
    public WifiOnlyException() {

    }

    public WifiOnlyException(String message) {
        super(message);
    }
}
