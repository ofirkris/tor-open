package co.we.torrent.other;

import android.content.Context;
import android.view.View;

import co.we.torrent.R;


/**
 * Created by mickael on 01/09/2016.
 */

public class ShowCaseInstance {
    private static final int USE_DEFAULT_TEXT_SIZE = -1;

    private View target;
    private String titleText;
    private String infoText;
    private int padding;
    private int infoTextSize;
    private boolean placeTextBelowView = false;
    private boolean isInfoTextBelowView = false;
    private boolean setGotItAboveView = false;

    private boolean customView = false;
    private int viewHeight = 1;
    private int viewWidth = 1;
    private int viewX = 0;
    private int viewY = 0;

    public ShowCaseInstance(View target, String titleText, String infoText, int padding, boolean placeTextBelowView) {
        this(target, titleText, infoText, padding, USE_DEFAULT_TEXT_SIZE, placeTextBelowView);
    }

    public ShowCaseInstance(View target, String titleText, String infoText, int padding, boolean placeTextBelowView, boolean isInfoTextBelowView) {
        this(target, titleText, infoText, padding, USE_DEFAULT_TEXT_SIZE, placeTextBelowView);
        this.isInfoTextBelowView = isInfoTextBelowView;

    }

    public ShowCaseInstance(View target, String titleText, String infoText, int padding, int infoTextSize, boolean placeTextBelowView) {
        this.target = target;
        this.titleText = titleText;
        this.infoText = infoText;
        this.padding = padding;

        if (infoTextSize == USE_DEFAULT_TEXT_SIZE)
            this.infoTextSize = 30;
        else
            this.infoTextSize = infoTextSize;

        this.placeTextBelowView = placeTextBelowView;
        isInfoTextBelowView = false;
    }

    public ShowCaseInstance(String titleText, String infoText, int padding, boolean placeTextBelowView, int viewHeight, int viewWidth, int viewX, int viewY) {
        this.titleText = titleText;
        this.infoText = infoText;
        this.padding = padding;
        if (target == null || target.getContext() == null)
            this.infoTextSize = 30;
        else {
            Context context = target.getContext();
            this.infoTextSize = (int) (context.getResources().getDimension(R.dimen.showcase_default_info_text_size));
        }
        this.placeTextBelowView = placeTextBelowView;
        this.viewHeight = viewHeight;
        this.viewWidth = viewWidth;
        this.viewX = viewX;
        this.viewY = viewY;
        this.customView = true;
        isInfoTextBelowView = false;
    }

    public View getTarget() {
        return target;
    }

    public void setTarget(View target) {
        this.target = target;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    public int getPadding() {
        return padding;
    }

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public int getInfoTextSize() {
        return infoTextSize;
    }

    public void setInfoTextSize(int infoTextSize) {
        this.infoTextSize = infoTextSize;
    }

    public boolean isPlaceTextBelowView() {
        return placeTextBelowView;
    }

    public void setPlaceNextSkipAboveView(boolean placeNextSkipAboveView) {
        this.placeTextBelowView = placeNextSkipAboveView;
    }

    public boolean isCustomView() {
        return customView;
    }

    public void setCustomView(boolean customView) {
        this.customView = customView;
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public void setViewHeight(int viewHeight) {
        this.viewHeight = viewHeight;
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public void setViewWidth(int viewWidth) {
        this.viewWidth = viewWidth;
    }

    public int getViewX() {
        return viewX;
    }

    public void setViewX(int viewX) {
        this.viewX = viewX;
    }

    public int getViewY() {
        return viewY;
    }

    public void setViewY(int viewY) {
        this.viewY = viewY;
    }

    public boolean isInfoTextBelowView() {
        return isInfoTextBelowView;
    }

    public void setInfoTextBelowView(boolean infoTextBelowView) {
        isInfoTextBelowView = infoTextBelowView;
    }

    public boolean isGotItAboveView() {
        return setGotItAboveView;
    }

    public void setSetGotItAboveView(boolean setGotItAboveView) {
        this.setGotItAboveView = setGotItAboveView;
    }
}
