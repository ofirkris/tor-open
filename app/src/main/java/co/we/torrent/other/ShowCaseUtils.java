package co.we.torrent.other;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.braunster.tutorialview.object.Tutorial;
import com.braunster.tutorialview.object.TutorialBuilder;
import com.braunster.tutorialview.object.TutorialIntentBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import co.we.torrent.R;


public class ShowCaseUtils {
    public static final int NO_REQUEST_CODE = -1;
    private static int ANIMATION_DURATION = 500;

    public static void showShowCaseSequence(final Activity activity, final Fragment fragment, final int requestCode, int delayTime, final ShowCaseInstance[] showcaseInstances) {
        showShowCaseSequence(activity, fragment, requestCode, delayTime, Arrays.asList(showcaseInstances));
    }

    public static void showShowCaseSequence(final Activity activity, Fragment fragment, final int requestCode, int delayTime, final List<ShowCaseInstance> showcaseInstances) {
        if (delayTime > 0) {
            new Handler().postDelayed(() -> showShowCaseView(activity, fragment, requestCode, showcaseInstances), delayTime);
        } else {
            showShowCaseView(activity, fragment, requestCode, showcaseInstances);
        }
    }

    private static void showShowCaseView(Activity activity, Fragment fragment, int requestCode, List<ShowCaseInstance> showcaseInstances) {
        if (showcaseInstances.isEmpty()) return;

        TutorialIntentBuilder builder = new TutorialIntentBuilder(activity);
        if (showcaseInstances.size() != 1) {
            ArrayList<Tutorial> tutorials = new ArrayList<>();
            for (ShowCaseInstance showcase : showcaseInstances) {
                Tutorial tutorial = createTutorial(activity, showcase, true, showcase == showcaseInstances.get(showcaseInstances.size() - 1));
                tutorials.add(tutorial);
            }
            builder.setWalkThroughList(tutorials);
        } else {
            Tutorial tutorial = createTutorial(activity, showcaseInstances.get(0), false, true);
            builder.setTutorial(tutorial);
        }

        // if true the status bar and navigation bar will be colored on Lollipop devices.
        builder.changeSystemUiColor(true);

        builder.skipTutorialOnBackPressed(true);

        Intent intent = builder.getIntent();
        if (fragment.isAdded()) {
            if (requestCode != NO_REQUEST_CODE) {
                fragment.startActivityForResult(intent, requestCode);
            } else {
                fragment.startActivity(intent);
            }
        }

        // Override the default animation of the entering activity.
        // This will allow the nice wrapping of the view by the tutorial activity.
        activity.overridePendingTransition(R.anim.dummy, R.anim.dummy);
    }

    private static Tutorial createTutorial(Context con, ShowCaseInstance showcase, boolean isWalkthrough, boolean isLastTutorialInSequence) {
        TutorialBuilder tBuilder = new TutorialBuilder();

        tBuilder.setTitle(showcase.getTitleText())
                .setViewToSurround(showcase.getTarget(), showcase.getPadding())
                .setInfoText(showcase.getInfoText())
                .setBackgroundColor(ContextCompat.getColor(con, R.color.transparent_black_color))
                .setTutorialTextColor(Color.WHITE)
                .setTutorialTextSize(showcase.getInfoTextSize())
                .setAnimationDuration(ANIMATION_DURATION);


        if (showcase.isCustomView()) {
            tBuilder.setmPositionToSurroundY(showcase.getViewY());
            tBuilder.setPositionToSurroundX(showcase.getViewX());
            tBuilder.setPositionToSurroundHeight(showcase.getViewHeight());
            tBuilder.setPositionToSurroundWidth(showcase.getViewWidth());
        }

        if (showcase.isInfoTextBelowView())
            tBuilder.setTutorialInfoTextPosition(Tutorial.InfoPosition.BELOW);


        String nextText = (isWalkthrough && !isLastTutorialInSequence) ?
                con.getString(R.string.showcase_next_it_text) : con.getString(R.string.showcase_got_it_text);

        String skipText = (isWalkthrough && !isLastTutorialInSequence) ?
                con.getString(R.string.showcase_skip_text) : "";

        if (showcase.isPlaceTextBelowView())
            tBuilder.setTutorialTextPosition(Tutorial.TextPosition.BELOW_VIEW);

        if (showcase.isGotItAboveView())
            tBuilder.setTutorialGotItPosition(Tutorial.GotItPosition.ABOVE_VIEW);


        tBuilder.setGotItText(nextText);
        tBuilder.setSkipText(skipText);

        return tBuilder.build();
    }

}
