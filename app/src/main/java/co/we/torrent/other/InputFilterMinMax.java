
package co.we.torrent.other;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Filtering numbers, which are outside the specified range.
 */

public class InputFilterMinMax implements InputFilter {
    private int min;
    private int max;

    public InputFilterMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public InputFilterMinMax(String min, String max) {
        this.min = Integer.parseInt(min);
        this.max = Integer.parseInt(max);
    }

    @Override
    public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
        try {
            int input = Integer.parseInt(spanned.toString() + charSequence.toString());
            if (inRange(min, max, input)) {
                return null;
            }

        } catch (NumberFormatException ignored) {
        }

        return "";
    }

    private boolean inRange(int first, int second, int value) {
        return second > first
                ? value >= first && value <= second
                : value >= second && value <= first;
    }
}
