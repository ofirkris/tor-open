package co.we.torrent.other;

import co.we.torrent.data.core.filetree.FileTree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * The modification of algorithm depth-first search for bypass BencodeFileTree.
 *
 * TODO: make non-recursive
 */

public class FileTreeDepthFirstSearch<F extends FileTree<F>>
{
    private List<F> leaves = new ArrayList<>();
    private Map<Integer, F> leavesAsMap = new HashMap<>();
    private int findIndex = -1;
    private F findResult;

    /*
     * Not recommended for use with a large number of nodes
     * due to deterioration of performance, use getLeaves() method
     */

    public F find(F node, int index)
    {
        if (node == null) {
            return null;
        }

        if (findIndex == -1) {
            findIndex = index;
        }

        if (node.getIndex() == findIndex) {
            findResult = node;
        } else {
            for (String n : node.getChildrenName()) {
                if (!node.isFile()) {
                    find(node.getChild(n), index);
                }
            }
        }

        findIndex = -1;

        return findResult;
    }

    /*
     * Returns the leaf nodes of the tree.
     */

    public List<F> getLeaves(F node)
    {
        if (node == null) {
            return null;
        }

        if (node.isFile()) {
            leaves.add(node);
        }

        for (F n : node.getChildren()) {
            if (!node.isFile()) {
                getLeaves(n);
            }
        }

        return leaves;
    }

    public Map<Integer, F> getLeavesAsMap(F node)
    {
        if (node == null) {
            return null;
        }

        if (node.isFile()) {
            leavesAsMap.put(node.getIndex(), node);
        }

        for (F n : node.getChildren()) {
            if (!node.isFile()) {
                getLeavesAsMap(n);
            }
        }

        return leavesAsMap;
    }
}
