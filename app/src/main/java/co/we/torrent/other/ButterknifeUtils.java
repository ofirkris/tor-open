package co.we.torrent.other;

import android.view.View;

import java.util.List;

import butterknife.ButterKnife;

/**
 * Created 22.10.2017 by
 *
 * @author Alexander Artemov
 */
public class ButterknifeUtils {

    private static final ButterKnife.Setter<View, Integer> VISIBILITY = (view, value, index) -> view.setVisibility(value);

    public static void setVisibility(List<View> views, int visibility) {
        if (views == null) return;
        ButterKnife.apply(views, VISIBILITY, visibility);
    }
}
