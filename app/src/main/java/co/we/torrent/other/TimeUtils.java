package co.we.torrent.other;

/**
 * Created 09.01.2018 by
 *
 * @author Alexander Artemov
 */
public class TimeUtils {
    public static String formatTime(long eta) {
        long min = eta  / 60 % 60;
        long h = eta  / 60 / 60;
        String hours = String.valueOf(h) + "h";
        String minutes = String.valueOf(min) + "m";
        if (h > 0) {
            if (h > 999) {
                return Utils.INFINITY_SYMBOL;
            } else {
                return hours + " " + minutes;
            }
        } else {
            return minutes;
        }
    }
}
