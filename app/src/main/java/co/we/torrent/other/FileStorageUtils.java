package co.we.torrent.other;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.io.File;

import co.we.torrent.R;
import timber.log.Timber;

/**
 * Created 22.10.2017 by
 *
 * @author Alexander Artemov
 */
public class FileStorageUtils {

    private static final int NOT_FOUND = -1;
    public static final char EXTENSION_SEPARATOR = '.';
    private static final char UNIX_SEPARATOR = '/';
    private static final char WINDOWS_SEPARATOR = '\\';

    private static boolean isExist(String path) {
        Timber.i("path ex %s result: %s", path, new File(path).exists());
        return new File(path).exists();
    }

    public static String getFileExtensionName(final String filename) {
        if (filename == null) {
            return null;
        }
        final int indexSeparator = indexOfLastSeparator(filename);

        return filename.substring(indexSeparator + 1);
    }


    public static String getFileName(final String filename) {
        if (filename == null) {
            return null;
        }
        final int indexExt = indexOfExtension(filename);
        final int indexSeparator = indexOfLastSeparator(filename);

        return filename.substring(indexSeparator + 1, indexExt);
    }

    public static String getExtension(final String path) {
        if (path == null) {
            return null;
        }
        final int index = indexOfExtension(path);
        if (index == NOT_FOUND) {
            return "";
        } else {
            return path.substring(index+1);
        }
    }

    public static int indexOfExtension(final String filename) {
        if (filename == null) {
            return NOT_FOUND;
        }
        final int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
        final int lastSeparator = indexOfLastSeparator(filename);
        return lastSeparator > extensionPos ? NOT_FOUND : extensionPos;
    }

    public static int indexOfLastSeparator(final String filename) {
        if (filename == null) {
            return NOT_FOUND;
        }

        final int lastUnixPos = filename.lastIndexOf(UNIX_SEPARATOR);
        final int lastWindowsPos = filename.lastIndexOf(WINDOWS_SEPARATOR);
        return Math.max(lastUnixPos, lastWindowsPos);
    }


    public static void openFile(File file, Context context, boolean forceChooser, boolean useNewStack) {
        Intent chooserIntent = new Intent();
        chooserIntent.setAction(Intent.ACTION_VIEW);

        String ext = FileStorageUtils.getExtension(file.getPath());

        String type = file.isDirectory()
                ? "*/*"
                : MimeTypes.getMimeType(ext);

        if (type != null && type.trim().length() != 0 && !type.equals("*/*")) {
            Uri uri = fileToUri(context, file);

            if (uri == null) uri = Uri.fromFile(file);
            chooserIntent.setDataAndType(uri, type);

            if (MimeTypes.isArchive(ext)) {
                sendZip(file, context, forceChooser, useNewStack, chooserIntent);
                return;
            }

            Intent activityIntent = getIntent(context, forceChooser, useNewStack, chooserIntent);
            try {
                context.startActivity(activityIntent);
            } catch (ActivityNotFoundException e) {
                openUnknownFile(file, context, forceChooser, useNewStack, chooserIntent);
            }
        } else {
            // failed to load mime type
            openUnknownFile(file, context, forceChooser, useNewStack, chooserIntent);
        }
    }

    private static void sendZip(File file, Context context, boolean forceChooser, boolean useNewStack, Intent chooserIntent) {
        try {
            context.startActivity(getIntent(context, forceChooser, useNewStack, chooserIntent));
        } catch (ActivityNotFoundException Ae) {
            //When No application can perform zip file
            Uri uri = Uri.parse("market://search?q=" + "application/zip");
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);

            try {
                context.startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                //the device hasn't installed Google Play
                openUnknownFile(file, context, forceChooser, useNewStack, chooserIntent);
            }
        }
    }

    private static Intent getIntent(Context context, boolean forceChooser, boolean useNewStack, Intent chooserIntent) {
        Intent activityIntent;
        if (forceChooser) {
            if (useNewStack) applyNewDocFlag(chooserIntent);
            activityIntent = Intent.createChooser(chooserIntent, context.getResources().getString(R.string.openwith));
        } else {
            activityIntent = chooserIntent;
            if (useNewStack) applyNewDocFlag(activityIntent);
        }
        return activityIntent;
    }

    private static void openUnknownFile(File file, Context context, boolean forceChooser, boolean useNewStack, Intent chooserIntent) {
        chooserIntent.setDataAndType(Uri.fromFile(file), "*/*");

        Intent activityIntent = getIntent(context, forceChooser, useNewStack, chooserIntent);
        try {
            context.startActivity(activityIntent);
        } catch (ActivityNotFoundException e) {
            Timber.e(e);
            Crashlytics.logException(e);
            Toast.makeText(context, R.string.noappfound, Toast.LENGTH_SHORT).show();
        }
    }

    private static void applyNewDocFlag(Intent i) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        } else {
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        }
    }

    private static final String INTERNAL = "internal";
    private static final String EXTERNAL = "external";

    private static final String EMULATED_STORAGE_SOURCE = System.getenv("EMULATED_STORAGE_SOURCE");
    private static final String EMULATED_STORAGE_TARGET = System.getenv("EMULATED_STORAGE_TARGET");
    private static final String EXTERNAL_STORAGE = System.getenv("EXTERNAL_STORAGE");

    private static String normalizeMediaPath(String path) {
        // Retrieve all the paths and check that we have this environment vars
        if (TextUtils.isEmpty(EMULATED_STORAGE_SOURCE) ||
                TextUtils.isEmpty(EMULATED_STORAGE_TARGET) ||
                TextUtils.isEmpty(EXTERNAL_STORAGE)) {
            return path;
        }

        // We need to convert EMULATED_STORAGE_SOURCE -> EMULATED_STORAGE_TARGET
        if (path.startsWith(EMULATED_STORAGE_SOURCE)) {
            path = path.replace(EMULATED_STORAGE_SOURCE, EMULATED_STORAGE_TARGET);
        }
        return path;
    }

    private static Uri fileToUri(Context context, File file) {
        // Normalize the path to ensure media ic_search
        final String normalizedPath = normalizeMediaPath(file.getAbsolutePath());

        // Check in external and internal storages
        Uri uri = getFileUri(context, normalizedPath, EXTERNAL);
        if (uri != null) {
            return uri;
        } else {
            return getFileUri(context, normalizedPath, INTERNAL);
        }
    }

    private static Uri getFileUri(Context context, String path, String value) {
        String[] projection;
        final String where = MediaStore.MediaColumns.DATA + " = ?";
        Uri baseUri = MediaStore.Files.getContentUri(value);
        boolean isMimeTypeVideo = false;
        boolean isMimeTypeAudio = false;
        boolean isMimeTypeImage = isPicture(path);

        if (!isMimeTypeImage) {
            isMimeTypeVideo = isVideo(path);
            if (!isMimeTypeVideo) {
                isMimeTypeAudio = isAudio(path);
            }
        }

        if (isMimeTypeImage || isMimeTypeVideo || isMimeTypeAudio) {
            projection = new String[]{BaseColumns._ID};
            if (isMimeTypeImage) {
                baseUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if (isMimeTypeVideo) {
                baseUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else {
                baseUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
        } else {
            projection = new String[]{BaseColumns._ID, MediaStore.Files.FileColumns.MEDIA_TYPE};
        }

        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(baseUri, projection, where, new String[]{path}, null);
        try {
            if (cursor != null && cursor.moveToNext()) {
                boolean isValid;
                if (isMimeTypeImage || isMimeTypeVideo || isMimeTypeAudio) {
                    isValid = true;
                } else {
                    int type = cursor.getInt(cursor.getColumnIndexOrThrow(
                            MediaStore.Files.FileColumns.MEDIA_TYPE));
                    isValid = type != 0;
                }

                if (isValid) {
                    // Do not force to use content uri for no media files
                    long id = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));
                    return Uri.withAppendedPath(baseUri, String.valueOf(id));
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private static boolean isAudio(String path) {
        return matchType("audio", MimeTypes.getMimeType(FileStorageUtils.getExtension(path)));
    }

    private static boolean isVideo(String path) {
        return matchType("video", MimeTypes.getMimeType(FileStorageUtils.getExtension(path)));
    }

    private static boolean isPicture(String path) {
        return matchType("image", MimeTypes.getMimeType(FileStorageUtils.getExtension(path)));
    }

    private static boolean matchType(String expectedType, String mimeType) {
        if (mimeType != null && mimeType.contains("/")) {
            final String typeOnly = mimeType.split("/")[0];

            return expectedType.equals(typeOnly);
        }
        return false;
    }
}
