package co.we.torrent.models;

/**
 * Created 07.01.2018 by
 *
 * @author Alexander Artemov
 */
public class TorrentListType {
    public static final int ALL = 0;
    public static final int QUEUED = 1;
    public static final int FINISHED = 2;
}
