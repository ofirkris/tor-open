package co.we.torrent.models;

/**
 * Created 16.01.2018 by
 *
 * @author Alexander Artemov
 */
public class AddError  {

    public static final int DECODE_EXCEPTION = 1;
    public static final int FETCH_LINK_EXCEPTION = 2;
    public static final int ILLEGAL_ARGUMENT_EXCEPTION = 3;
    public static final int IO_EXCEPTION = 4;
    public static final int SPACE_EXCEPTION = 5;
    public static final int EXCEPTION = 0;

}
