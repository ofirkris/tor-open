package co.we.torrent.di.app;


import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;
import co.we.torrent.di.scopes.Main;
import co.we.torrent.models.TorrentListType;
import co.we.torrent.navigation.AppRouter;
import co.we.torrent.presentation.main.presenter.TorrentsPresenter;

/**
 * Created 13.12.2017 by
 *
 * @author Alexander Artemov
 */
@Module
public class MainModule {

    @Main
    @Provides
    @IntoMap
    @IntKey(TorrentListType.ALL)
    public TorrentsPresenter provideAllPresenter(AppRouter appRouter) {
        return new TorrentsPresenter(appRouter, TorrentListType.ALL);
    }

    @Main
    @Provides
    @IntoMap
    @IntKey(TorrentListType.QUEUED)
    public TorrentsPresenter provideQueuedPresenter(AppRouter appRouter) {
        return new TorrentsPresenter(appRouter, TorrentListType.QUEUED);
    }

    @Main
    @Provides
    @IntoMap
    @IntKey(TorrentListType.FINISHED)
    public TorrentsPresenter provideFinishedPresenter(AppRouter appRouter) {
        return new TorrentsPresenter(appRouter, TorrentListType.FINISHED);
    }
}
