package co.we.torrent.di.app;

import javax.inject.Singleton;

import co.we.torrent.presentation.base.BaseActivity;
import dagger.Component;

/**
 * @author Alexander Artemov
 */
@Component(modules = {AppContextModule.class, NavigationModule.class})
@Singleton
public interface AppComponent {
    MainComponent plusMainComponent();

    void inject(BaseActivity baseActivity);

}
