package co.we.torrent.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created 21.11.2017 by
 *
 * @author Alexander Artemov
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface Main {
}
