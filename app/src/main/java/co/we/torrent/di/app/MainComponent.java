package co.we.torrent.di.app;

import co.we.torrent.di.scopes.Main;
import co.we.torrent.presentation.main.presenter.MainPresenter;
import co.we.torrent.presentation.main.presenter.TorrentsPresenter;
import co.we.torrent.presentation.main.ui.MainFragment;
import co.we.torrent.presentation.main.ui.TorrentsFragment;

import java.util.Map;

import dagger.Subcomponent;

/**
 * Created 13.12.2017 by
 *
 * @author Alexander Artemov
 */
@Main
@Subcomponent(modules = {MainModule.class})
public interface MainComponent {
    Map<Integer, TorrentsPresenter> presenters();

    MainPresenter getMainPresenter();

    void inject(MainFragment mainFragment);

    void inject(TorrentsFragment torrentsFragment);
}
