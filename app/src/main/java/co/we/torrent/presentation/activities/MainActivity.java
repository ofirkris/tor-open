package co.we.torrent.presentation.activities;


import android.content.Intent;
import android.os.Bundle;

import co.we.torrent.data.receivers.NotificationReceiver;
import co.we.torrent.data.services.TorrentTaskService;
import co.we.torrent.navigation.Screens;
import co.we.torrent.presentation.base.BaseActivity;
import co.we.torrent.presentation.old.fragments.FragmentCallback;
import ru.terrakok.cicerone.commands.Replace;

/**
 * Created 08.01.2018 by
 *
 * @author Alexander Artemov
 */
public class MainActivity extends BaseActivity implements FragmentCallback {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String action = getIntent().getAction();
        if (action != null && action.equals(NotificationReceiver.NOTIFY_ACTION_SHUTDOWN_APP)) {
            finish();
            return;
        }

        startService(new Intent(this, TorrentTaskService.class));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void openFirstScreen() {
        navigator.applyCommand(new Replace(Screens.MAIN, null));
    }

    @Override
    public void fragmentFinished(Intent intent, ResultCode code) {
        switch (code) {
            case OK:
                finish();
                break;
            case CANCEL:
            case BACK:
                break;
        }
    }
}