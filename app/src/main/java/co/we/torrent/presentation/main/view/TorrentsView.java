package co.we.torrent.presentation.main.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface TorrentsView extends MvpView {

    @StateStrategyType(AddToEndSingleStrategy.class)
    void toggleDeleteMode(boolean value);
}
