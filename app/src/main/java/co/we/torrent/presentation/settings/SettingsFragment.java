package co.we.torrent.presentation.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.preference.Preference;

import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

import co.we.torrent.R;
import co.we.torrent.other.Utils;
import co.we.torrent.presentation.activities.SettingsActivity;

public class SettingsFragment extends PreferenceFragmentCompat {

    private Callback callback;

    public interface Callback {
        void onDetailTitleChanged(String title);
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();

        fragment.setArguments(new Bundle());

        return fragment;
    }

    /* For API < 23 */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof Callback) {
            callback = (Callback) activity;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        callback = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            setFragment(BehaviorSettingsFragment.newInstance(),
                    getString(R.string.pref_header_behavior));
        }

        FragmentActivity activity = getActivity();
        if (activity == null) return;

        Context context = activity.getApplicationContext();

        Preference behavior = findPreference(BehaviorSettingsFragment.class.getSimpleName());
        behavior.setOnPreferenceClickListener(preference -> {
            if (Utils.isTablet(context)) {
                setFragment(BehaviorSettingsFragment.newInstance(),
                        getString(R.string.pref_header_behavior));
            } else {
                startActivity(BehaviorSettingsFragment.class,
                        getString(R.string.pref_header_behavior));
            }

            return true;
        });

        Preference storage = findPreference(StorageSettingsFragment.class.getSimpleName());
        storage.setOnPreferenceClickListener(preference -> {
            if (Utils.isTablet(context)) {
                setFragment(StorageSettingsFragment.newInstance(),
                        getString(R.string.pref_header_storage));
            } else {
                startActivity(StorageSettingsFragment.class,
                        getString(R.string.pref_header_storage));
            }

            return true;
        });

        Preference limitations = findPreference(LimitationsSettingsFragment.class.getSimpleName());
        limitations.setOnPreferenceClickListener(preference -> {
            if (Utils.isTablet(context)) {
                setFragment(LimitationsSettingsFragment.newInstance(),
                        getString(R.string.pref_header_limitations));
            } else {
                startActivity(LimitationsSettingsFragment.class,
                        getString(R.string.pref_header_limitations));
            }

            return true;
        });

        Preference network = findPreference(NetworkSettingsFragment.class.getSimpleName());
        network.setOnPreferenceClickListener(preference -> {
            if (Utils.isTablet(context)) {
                setFragment(NetworkSettingsFragment.newInstance(),
                        getString(R.string.pref_header_network));
            } else {
                startActivity(NetworkSettingsFragment.class,
                        getString(R.string.pref_header_network));
            }

            return true;
        });
    }

    @Override
    public void onCreatePreferencesFix(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.pref_headers, rootKey);
    }

    private <F extends PreferenceFragmentCompat> void setFragment(F fragment, String title) {
        SettingsActivity activity = (SettingsActivity) getActivity();
        if (activity == null) return;

        if (Utils.isTablet(activity.getApplicationContext()) && activity.isTablet()) {
            if (callback != null) {
                callback.onDetailTitleChanged(title);
            }

            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.detail_fragment_container, fragment)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private <F extends PreferenceFragmentCompat> void startActivity(Class<F> fragment, String title) {
        Intent i = new Intent(getActivity(), BasePreferenceActivity.class);
        PreferenceActivityConfig config = new PreferenceActivityConfig(
                fragment.getSimpleName(),
                title);

        i.putExtra(BasePreferenceActivity.TAG_CONFIG, config);
        startActivity(i);
    }
}
