package co.we.torrent.presentation.old.dialogs.filemanager;

import co.we.torrent.data.core.filetree.FileNode;

/*
 * The class encapsulates a node and properties, that determine whether he is a file or directory.
 */

public class FileManagerNode implements FileNode<FileManagerNode>
{
    public static final String PARENT_DIR = "..";
    public static final String ROOT_DIR = "/";

    private String node;
    private int nodeType;

    public FileManagerNode(String item, int itemType)
    {
        node = item;
        nodeType = itemType;
    }

    @Override
    public String getName()
    {
        return node;
    }

    @Override
    public void setName(String name)
    {
        node = name;
    }

    @Override
    public int getType()
    {
        return nodeType;
    }

    @Override
    public void setType(int type)
    {
        nodeType = type;
    }

    @Override
    public int compareTo(FileManagerNode another)
    {
        return node.compareTo(another.getName());
    }

    @Override
    public String toString()
    {
        return "FileManagerNode{" +
                "node='" + node + '\'' +
                ", nodeType=" + nodeType +
                '}';
    }
}
