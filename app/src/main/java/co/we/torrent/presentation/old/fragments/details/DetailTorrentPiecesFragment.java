package co.we.torrent.presentation.old.fragments.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import co.we.torrent.R;
import co.we.torrent.presentation.custom_views.PiecesView;

/**
 * The fragment for displaying torrent pieces. Part of DetailTorrentFragment.
 */

public class DetailTorrentPiecesFragment extends Fragment {

    private static final String TAG_PIECES = "pieces";
    private static final String TAG_ALL_PIECES_COUNT = "all_pieces_count";
    private static final String TAG_PIECE_SIZE = "piece_size";
    private static final String TAG_DOWNLOADED_PIECES = "downloaded_pieces";
    private static final String TAG_SCROLL_POSITION = "scroll_position";

    private AppCompatActivity activity;
    private PiecesView pieceMap;
    private TextView piecesCounter;
    private NestedScrollView pieceMapScrollView;

    private boolean[] pieces;
    private int allPiecesCount;
    private int pieceSize;
    private int downloadedPieces;
    private int[] scrollPosition = new int[]{0, 0};

    public static DetailTorrentPiecesFragment newInstance(int allPiecesCount, int pieceSize) {
        DetailTorrentPiecesFragment fragment = new DetailTorrentPiecesFragment();

        fragment.allPiecesCount = allPiecesCount;
        fragment.pieceSize = pieceSize;

        fragment.setArguments(new Bundle());

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity) {
            this.activity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            pieces = savedInstanceState.getBooleanArray(TAG_PIECES);
            allPiecesCount = savedInstanceState.getInt(TAG_ALL_PIECES_COUNT);
            pieceSize = savedInstanceState.getInt(TAG_PIECE_SIZE);
            downloadedPieces = savedInstanceState.getInt(TAG_DOWNLOADED_PIECES);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fmt_detail_torrent_pieces, container, false);

        pieceMap = (PiecesView) v.findViewById(R.id.piece_map);
        piecesCounter = (TextView) v.findViewById(R.id.pieces_count);
        pieceMapScrollView = (NestedScrollView) v.findViewById(R.id.piece_map_scroll_view);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (activity == null) {
            activity = (AppCompatActivity) getActivity();
        }

        pieceMap.setPieces(pieces);
        updatePieceCounter();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBooleanArray(TAG_PIECES, pieces);
        outState.putInt(TAG_ALL_PIECES_COUNT, allPiecesCount);
        outState.putInt(TAG_PIECE_SIZE, pieceSize);
        outState.putInt(TAG_DOWNLOADED_PIECES, downloadedPieces);
        scrollPosition[0] = pieceMapScrollView.getScrollX();
        scrollPosition[1] = pieceMapScrollView.getScrollY();
        outState.putIntArray(TAG_SCROLL_POSITION, scrollPosition);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            scrollPosition = savedInstanceState.getIntArray(TAG_SCROLL_POSITION);
            if (scrollPosition != null && scrollPosition.length == 2) {
                pieceMapScrollView.scrollTo(scrollPosition[0], scrollPosition[1]);
            }
        }
    }

    public void setPieces(boolean[] pieces) {
        if (pieces == null) {
            return;
        }

        this.pieces = pieces;
        pieceMap.setPieces(pieces);
    }

    public void setPiecesCountAndSize(int allPiecesCount, int pieceSize) {
        this.allPiecesCount = allPiecesCount;
        this.pieceSize = pieceSize;

        updatePieceCounter();
    }

    public void setDownloadedPiecesCount(int downloadedPieces) {
        this.downloadedPieces = downloadedPieces;

        updatePieceCounter();
    }

    private void updatePieceCounter() {
        String piecesTemplate = activity.getString(R.string.torrent_pieces_template);
        String pieceLength = Formatter.formatFileSize(activity, pieceSize);
        piecesCounter.setText(
                String.format(piecesTemplate,
                        downloadedPieces,
                        allPiecesCount,
                        pieceLength));
    }
}
