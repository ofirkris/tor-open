package co.we.torrent.presentation.old.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

/**
 * The simple spinner progress dialog.
 */
public class SpinnerProgressDialog extends DialogFragment {
    protected static final String TAG_TITLE = "title";
    protected static final String TAG_MESSAGE = "message";
    protected static final String TAG_PROGRESS = "progress";
    protected static final String TAG_IS_INDETERMINATE = "is_indeterminate";
    protected static final String TAG_IS_CANCELABLE = "is_cancelable";

    /* In the absence of any parameter need set 0 */

    public static SpinnerProgressDialog newInstance(int title, String message, int progress,
                                                    boolean isIndeterminate, boolean isCancelable) {
        SpinnerProgressDialog frag = new SpinnerProgressDialog();

        Bundle args = new Bundle();

        args.putInt(TAG_TITLE, title);
        args.putString(TAG_MESSAGE, message);
        args.putInt(TAG_PROGRESS, progress);
        args.putBoolean(TAG_IS_INDETERMINATE, isIndeterminate);
        args.putBoolean(TAG_IS_CANCELABLE, isCancelable);

        frag.setArguments(args);

        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        int title = args.getInt(TAG_TITLE);
        String message = args.getString(TAG_MESSAGE);
        int progress = args.getInt(TAG_PROGRESS);
        boolean isIndeterminate = args.getBoolean(TAG_IS_INDETERMINATE);
        boolean isCancelable = args.getBoolean(TAG_IS_CANCELABLE);

        ProgressDialog dialog = new ProgressDialog(getActivity());
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setIndeterminate(isIndeterminate);
        dialog.setCancelable(isCancelable);
        dialog.setProgressNumberFormat(null);
        dialog.setProgressPercentFormat(null);
        dialog.setProgress(progress);

        return dialog;
    }
}
