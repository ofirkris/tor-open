
package co.we.torrent.presentation.old.adapters;

import android.support.v7.widget.RecyclerView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import co.we.torrent.data.core.filetree.FileNode;

/*
 * The base adapter, generalizing operations on file lists.
 */

public abstract class BaseFileListAdapter<VH extends RecyclerView.ViewHolder, F extends FileNode>
        extends SelectableAdapter<VH> {
    @SuppressWarnings("unused")
    private static final String TAG = BaseFileListAdapter.class.getSimpleName();

    protected List<F> files;

    public synchronized void addFiles(Collection<F> files) {
        this.files.addAll(files);
        Collections.sort(this.files);
        notifyItemRangeInserted(0, files.size() - 1);
    }

    public void clearFiles() {
        int size = files.size();
        if (size > 0) {
            files.clear();

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void deleteFiles(Collection<F> files) {
        this.files.removeAll(files);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return files.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return files == null ? 0 : files.size();
    }

    public boolean isEmpty() {
        return files.isEmpty();
    }
}
