package co.we.torrent.presentation.main.ui;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created 25.11.2017 by
 *
 * @author Alexander Artemov
 */
public class MainPagerAdapter extends FragmentStatePagerAdapter {

    private final Fragment[] pages;
    private final String[] titles;

    public MainPagerAdapter(FragmentManager fm, Fragment[] fragments, String[] titles) {
        super(fm);
        this.pages = fragments;
        this.titles = titles;
    }

    @Override
    public Fragment getItem(int position) {
        return pages[position];
    }

    @Override
    public int getCount() {
        return pages.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
