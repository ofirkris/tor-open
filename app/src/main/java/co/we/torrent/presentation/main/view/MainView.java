package co.we.torrent.presentation.main.view;

import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import co.we.torrent.presentation.base.BaseMvpView;

public interface MainView extends BaseMvpView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void toggleDeleteMode(boolean isDeleteMode);

    @StateStrategyType(AddToEndSingleStrategy.class)
    void toggleSearchMode(boolean isSearchMode);

    @StateStrategyType(OneExecutionStateStrategy.class)
    void showTutorial(boolean showDialogAfterTutorial, int delay);
}
