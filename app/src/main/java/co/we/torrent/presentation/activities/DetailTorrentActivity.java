package co.we.torrent.presentation.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

import co.we.torrent.R;
import co.we.torrent.presentation.old.fragments.DetailTorrentFragment;
import co.we.torrent.presentation.old.fragments.FragmentCallback;

public class DetailTorrentActivity extends AppCompatActivity implements DetailTorrentFragment.Callback, FragmentCallback {

    public static final String TAG_TORRENT_ID = "torrent_id";

    private DetailTorrentFragment detailTorrentFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_detail_torrent);

        detailTorrentFragment = (DetailTorrentFragment) getSupportFragmentManager()
                .findFragmentById(R.id.detail_torrent_fragmentContainer);

        String id = getIntent().getStringExtra(TAG_TORRENT_ID);

        detailTorrentFragment.setTorrentId(id);
    }

    @Override
    public void onTorrentInfoChanged() {
        if (detailTorrentFragment != null) {
            detailTorrentFragment.onTorrentInfoChanged();
        }
    }

    @Override
    public void onTorrentPlayPauseClicked() {
        if (detailTorrentFragment != null) {
            detailTorrentFragment.pauseResumeTorrentRequest();
        }
    }

    @Override
    public void onTorrentInfoChangesUndone() {
        if (detailTorrentFragment != null) {
            detailTorrentFragment.onTorrentInfoChangesUndone();
        }
    }

    @Override
    public void onTorrentFilesChanged() {
        if (detailTorrentFragment != null) {
            detailTorrentFragment.onTorrentFilesChanged();
        }
    }

    @Override
    public void onTrackersChanged(ArrayList<String> trackers, boolean replace) {
        if (detailTorrentFragment != null) {
            detailTorrentFragment.onTrackersChanged(trackers, replace);
        }
    }

    @Override
    public void openFile(String relativePath) {
        if (detailTorrentFragment != null) {
            detailTorrentFragment.openFile(relativePath);
        }
    }

    @Override
    public void fragmentFinished(Intent intent, ResultCode code) {
        finish();
    }

    @Override
    public void onBackPressed() {
        detailTorrentFragment.onBackPressed();
    }
}
