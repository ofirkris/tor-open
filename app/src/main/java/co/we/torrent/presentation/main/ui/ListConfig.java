package co.we.torrent.presentation.main.ui;


import javax.inject.Inject;

import co.we.torrent.di.scopes.Main;

/**
 * Created 14.12.2017 by
 *
 * @author Alexander Artemov
 */
@Main
public class ListConfig {

    private boolean deleteMode;
    private boolean searchMode;

    @Inject
    public ListConfig() {
    }

    public boolean isLocked() {
        return isDeleteMode() || isSearchMode();
    }

    public boolean isDeleteMode() {
        return deleteMode;
    }

    public void setDeleteMode(boolean isDeleteMode) {
        this.deleteMode = isDeleteMode;
    }

    public boolean isSearchMode() {
        return searchMode;
    }

    public void setSearchMode(boolean searchMode) {
        this.searchMode = searchMode;
    }
}
