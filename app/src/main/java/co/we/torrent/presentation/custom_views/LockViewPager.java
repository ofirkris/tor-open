package co.we.torrent.presentation.custom_views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import co.we.torrent.presentation.main.ui.ListConfig;

/**
 * Created 15.12.2017 by
 *
 * @author Alexander Artemov
 */
public class LockViewPager extends ViewPager {
    private ListConfig listConfig;

    public LockViewPager(Context context) {
        super(context);
    }

    public LockViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !listConfig.isLocked() && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        boolean isLocked = listConfig.isLocked();
        try {
            return !isLocked && super.onInterceptTouchEvent(event);
        } catch (Exception e){
            return isLocked;
        }
    }

    public void setListConfig(ListConfig listConfig) {
        this.listConfig = listConfig;
    }
}
