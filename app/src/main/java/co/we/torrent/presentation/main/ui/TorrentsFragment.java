package co.we.torrent.presentation.main.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.we.torrent.AndroidApplication;
import co.we.torrent.R;
import co.we.torrent.data.core.TorrentServiceCallback;
import co.we.torrent.data.core.sorting.TorrentSortingComparator;
import co.we.torrent.data.core.stateparcel.TorrentStateParcel;
import co.we.torrent.data.services.TorrentTaskService;
import co.we.torrent.other.Utils;
import co.we.torrent.presentation.base.BaseFragment;
import co.we.torrent.presentation.custom_views.EmptyRecyclerView;
import co.we.torrent.presentation.custom_views.RecyclerViewDividerDecoration;
import co.we.torrent.presentation.main.presenter.TorrentsPresenter;
import co.we.torrent.presentation.main.view.TorrentsView;

public class TorrentsFragment extends BaseFragment implements TorrentsView {
    public static TorrentsFragment newInstance(int currentType) {
        TorrentsFragment fragment = new TorrentsFragment();

        Bundle args = new Bundle();
        args.putInt(TYPE, currentType);
        fragment.setArguments(args);
        return fragment;
    }

    public static final String TYPE = "list_type";
    private static final String TAG_SELECTABLE_ADAPTER = "selectable_adapter";
    private static final String TAG_SELECTED_TORRENTS = "selected_torrents";
    private static final String TAG_TORRENTS_LIST_STATE = "torrents_list_state";

    @InjectPresenter TorrentsPresenter presenter;
    @Inject ListListenerContainer container;

    @BindView(R.id.list) EmptyRecyclerView torrentsList;
    @BindView(R.id.empty_view_torrent_list) View emptyView;

    private AppCompatActivity activity;
    private Unbinder unbinder;
    private LinearLayoutManager layoutManager;
    /* Save state scrolling */
    private Parcelable torrentsListState;
    private Map<String, TorrentStateParcel> torrentStates = new HashMap<>();
    private TorrentsAdapter adapter;

    private TorrentTaskService service;
    private boolean bound; //Flag indicating whether we have called bind on the service.
    private ReentrantLock callbackSync;
    private ReentrantLock adapterSync;

    private int currentType = -1;

    /**
     * --------------------------------- Initialization ------------------------------------------//
     */
    @ProvidePresenter
    public TorrentsPresenter providePresenter() {
        initializeCurrentType();
        return AndroidApplication.mainComponent().presenters().get(currentType);
    }

    private void initializeCurrentType() {
        currentType = getArguments().getInt(TYPE, -1);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fmt_torrents, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (activity == null) {
            activity = (AppCompatActivity) getActivity();
        }

        AndroidApplication.mainComponent().inject(this);

        initializeLocks();
        initializeService();
        initializeList();
        initializeCurrentType();
        initializeAdapter(savedInstanceState);
    }

    private void initializeLocks() {
        callbackSync = new ReentrantLock();
        adapterSync = new ReentrantLock();
    }

    private void initializeService() {
        activity.bindService(new Intent(activity.getApplicationContext(), TorrentTaskService.class),
                connection, Context.BIND_AUTO_CREATE);
    }

    private void initializeList() {
        layoutManager = new LinearLayoutManager(activity);
        torrentsList.setLayoutManager(layoutManager);

        /*
         * A RecyclerView by default creates another copy of the ViewHolder in order to
         * fade the views into each other. This causes the problem because the old ViewHolder gets
         * the payload but then the new one doesn't. So needs to explicitly tell it to reuse the old one.
         */
        DefaultItemAnimator animator = new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                return true;
            }
        };

        TypedArray a = activity.obtainStyledAttributes(new TypedValue().data, new int[]{R.attr.divider});

        torrentsList.setItemAnimator(animator);
        torrentsList.addItemDecoration(new RecyclerViewDividerDecoration(a.getDrawable(0)));
        torrentsList.setEmptyView(emptyView);

        a.recycle();
    }

    private void initializeAdapter(Bundle savedInstanceState) {
        adapter = new TorrentsAdapter(activity, TorrentsAdapter.DisplayFilter.create(currentType), container,
                new TorrentSortingComparator(Utils.getTorrentSorting(activity.getApplicationContext())));

        torrentsList.setAdapter(adapter);

        if (savedInstanceState != null) {
            adapter.setSelectedList(savedInstanceState.getStringArrayList(TAG_SELECTED_TORRENTS));
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (torrentsListState != null) {
            layoutManager.onRestoreInstanceState(torrentsListState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putIntegerArrayList(TAG_SELECTABLE_ADAPTER, adapter.getSelectedItems());
        outState.putStringArrayList(TAG_SELECTED_TORRENTS, adapter.getSelectedList());
        torrentsListState = layoutManager.onSaveInstanceState();
        outState.putParcelable(TAG_TORRENTS_LIST_STATE, torrentsListState);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        if (savedInstanceState != null) {
            torrentsListState = savedInstanceState.getParcelable(TAG_TORRENTS_LIST_STATE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (bound) {
            if (service != null) {
                service.removeListener(serviceCallback);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                getActivity().unbindService(connection);
            }
            bound = false;
        }


        unbinder.unbind();
    }

    /**
     * --------------------------------- Torrent service------------------------------------------//
     */

    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            TorrentTaskService.LocalBinder binder = (TorrentTaskService.LocalBinder) service;
            TorrentsFragment.this.service = binder.getService();
            TorrentsFragment.this.service.addListener(serviceCallback);
            bound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            TorrentsFragment.this.service.removeListener(serviceCallback);
            bound = false;
        }
    };

    TorrentServiceCallback serviceCallback = new TorrentServiceCallback() {
        @Override
        public void onTorrentStateChanged(TorrentStateParcel state) {
            callbackSync.lock();

            try {
                if (state != null) {
                    torrentStates.put(state.torrentId, state);
                    reloadAdapterItem(state);
                }

            } finally {
                callbackSync.unlock();
            }
        }

        @Override
        public void onTorrentsStateChanged(Bundle states) {
            if (states == null) {
                return;
            }

            callbackSync.lock();

            try {
                torrentStates.clear();

                for (String key : states.keySet()) {
                    TorrentStateParcel state = states.getParcelable(key);
                    if (state != null) {
                        torrentStates.put(state.torrentId, state);
                    }
                }

                reloadAdapter();

            } finally {
                callbackSync.unlock();
            }
        }

        @Override
        public void onTorrentAdded(TorrentStateParcel state) {
            callbackSync.lock();
            try {
                if (state != null) {
                    torrentStates.put(state.torrentId, state);
                    reloadAdapter();
                }
            } finally {
                callbackSync.unlock();
            }
        }

        @Override
        public void onTorrentRemoved(TorrentStateParcel state) {
            callbackSync.lock();

            try {
                torrentStates.remove(state.torrentId);
                deleteAdapterItem(state);

            } finally {
                callbackSync.unlock();
            }
        }
    };

    /**
     * ------------------------------------- Adapter ---------------------------------------------//
     */

    private void deleteAdapterItem(final TorrentStateParcel state) {
        adapterSync.lock();

        try {
            activity.runOnUiThread(() -> adapter.deleteItem(state));

        } finally {
            adapterSync.unlock();
        }
    }

    private void reloadAdapterItem(final TorrentStateParcel state) {
        adapterSync.lock();

        try {
            activity.runOnUiThread(() -> adapter.updateItem(state));

        } finally {
            adapterSync.unlock();
        }
    }

    final synchronized void reloadAdapter() {
        activity.runOnUiThread(() -> {
            adapter.clearAll();

            if (torrentStates == null || torrentStates.size() == 0) {
                adapter.notifyDataSetChanged();
            } else {
                adapter.addItems(torrentStates.values());
            }
        });
    }

    public TorrentsAdapter getAdapter() {
        return (TorrentsAdapter) torrentsList.getAdapter();
    }

    @Override
    public void toggleDeleteMode(boolean value) {
        if (!value) {
            adapter.clearSelectedList();
        }
    }
}
