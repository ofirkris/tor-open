package co.we.torrent.presentation.old.fragments;

import android.content.Intent;

/*
 * The basic callback interface with codes and functions, returned by fragments.
 */

public interface FragmentCallback {
    @SuppressWarnings("unused")
    String TAG = FragmentCallback.class.getSimpleName();

    enum ResultCode {
        OK, CANCEL, BACK
    }

    void fragmentFinished(Intent intent, ResultCode code);
}