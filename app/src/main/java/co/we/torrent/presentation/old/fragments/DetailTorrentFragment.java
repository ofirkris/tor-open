package co.we.torrent.presentation.old.fragments;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.frostwire.jlibtorrent.Priority;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import co.we.torrent.R;
import co.we.torrent.data.core.BencodeFileItem;
import co.we.torrent.data.core.Torrent;
import co.we.torrent.data.core.TorrentMetaInfo;
import co.we.torrent.data.core.TorrentServiceCallback;
import co.we.torrent.data.core.TorrentStateCode;
import co.we.torrent.data.core.exceptions.WifiOnlyException;
import co.we.torrent.data.core.stateparcel.PeerStateParcel;
import co.we.torrent.data.core.stateparcel.StateParcelCache;
import co.we.torrent.data.core.stateparcel.TorrentStateParcel;
import co.we.torrent.data.core.stateparcel.TrackerStateParcel;
import co.we.torrent.data.core.storage.TorrentStorage;
import co.we.torrent.data.services.TorrentTaskService;
import co.we.torrent.other.FileIOUtils;
import co.we.torrent.other.InputFilterMinMax;
import co.we.torrent.other.TorrentUtils;
import co.we.torrent.other.Utils;
import co.we.torrent.presentation.old.adapters.ViewPagerAdapter;
import co.we.torrent.presentation.old.dialogs.BaseAlertDialog;
import co.we.torrent.presentation.old.dialogs.filemanager.FileManagerConfig;
import co.we.torrent.presentation.old.dialogs.filemanager.FileManagerDialog;
import co.we.torrent.presentation.old.fragments.details.DetailTorrentInfoFragment;
import co.we.torrent.presentation.old.fragments.details.DetailTorrentPeersFragment;
import co.we.torrent.presentation.old.fragments.details.DetailTorrentPiecesFragment;
import co.we.torrent.presentation.old.fragments.details.DetailTorrentStateFragment;
import co.we.torrent.presentation.old.fragments.details.DetailTorrentTrackersFragment;
import timber.log.Timber;

public class DetailTorrentFragment extends Fragment implements BaseAlertDialog.OnClickListener,
        BaseAlertDialog.OnDialogShowListener {

    private static final String TAG_TORRENT_ID = "torrent_id";
    private static final String TAG_TORRENT_INFO_CHANGED = "torrent_info_changed";
    private static final String TAG_TORRENT_FILES_CHANGED = "torrent_files_changed";
    private static final String TAG_CHILD_IN_ACTION_MODE = "child_in_action_mode";
    private static final String TAG_ADD_TRACKERS_DIALOG = "add_trackers_dialog";
    private static final String TAG_DELETE_TORRENT_DIALOG = "delete_torrent_dialog";
    private static final String TAG_SPEED_LIMIT_DIALOG = "speed_limit_dialog";
    private static final String TAG_CURRENT_FRAG_POS = "current_frag_pos";

    private static final int SAVE_TORRENT_FILE_CHOOSE_REQUEST = 1;

    private static final int SYNC_TIME = 1000; /* ms */
    private static final int INFO_FRAG_POS = 0;
    private static final int STATE_FRAG_POS = 1;
    private static final int FILE_FRAG_POS = 2;
    private static final int TRACKERS_FRAG_POS = 3;
    private static final int PEERS_FRAG_POS = 4;
    private static final int PIECES_FRAG_POS = 5;

    private AppCompatActivity activity;

    @BindView(R.id.detail_toolbar) Toolbar toolbar;
    @BindView(R.id.coordinator_layout) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.detail_torrent_tabs) TabLayout tabLayout;
    @BindView(R.id.save_changes_button) FloatingActionButton saveChangesButton;
    @BindView(R.id.detail_torrent_viewpager) ViewPager viewPager;


    private ViewPagerAdapter adapter;
    private Unbinder unbinder;

    private String torrentId;
    private Torrent torrent;
    private TorrentStorage repo;
    private TorrentTaskService service;
    /* Flag indicating whether we have called bind on the service. */
    private boolean bound;
    /* Update the torrent status params, which aren't included in TorrentStateParcel */
    private Handler updateTorrentStateHandler = new Handler();
    Runnable updateTorrentState = new Runnable() {
        @Override
        public void run() {
            if (bound && service != null && torrentId != null) {
                getActiveAndSeedingTimeRequest();
                getTrackersStatesRequest();
                getPeerStatesRequest();
                getPiecesRequest();
            }
            updateTorrentStateHandler.postDelayed(this, SYNC_TIME);
        }
    };

    private boolean isTorrentInfoChanged = false;
    private boolean isTorrentFilesChanged = false;
    /* One of the fragments in ViewPagerAdapter is in ActionMode */
    private boolean childInActionMode = false;
    private int currentFragPos = INFO_FRAG_POS;

    /*
     * Caching data, if a fragment in tab isn't into view,
     * thereby preventing a useless update data in hidden fragments
     */
    private TorrentMetaInfo infoCache;
    private TorrentStateParcel stateCache;
    private long activeTimeCache, seedingTimeCache;
    private StateParcelCache<TrackerStateParcel> trackersCache = new StateParcelCache<>();
    private StateParcelCache<PeerStateParcel> peersCache = new StateParcelCache<>();
    private boolean[] piecesCache;
    private int uploadSpeedLimit = -1;
    private int downloadSpeedLimit = -1;
    private boolean downloadingMetadata = false;

    private boolean fragmentOpenedFirst = true;

    public interface Callback {
        void onTorrentInfoChanged();

        void onTorrentPlayPauseClicked();

        void onTorrentInfoChangesUndone();

        void onTorrentFilesChanged();

        void onTrackersChanged(ArrayList<String> trackers, boolean replace);

        void openFile(String relativePath);
    }

    public static DetailTorrentFragment newInstance(String id) {
        DetailTorrentFragment fragment = new DetailTorrentFragment();
        fragment.torrentId = id;

        fragment.setArguments(new Bundle());

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fmt_detail_torrent, container, false);
        unbinder = ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppCompatActivity) {
            activity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        stopUpdateTorrentState();
    }

    @Override
    public void onResume() {
        super.onResume();

        startUpdateTorrentState();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager != null ? fragmentManager.findFragmentByTag(TAG_DELETE_TORRENT_DIALOG) : null;

        /* Prevents leak the dialog in portrait mode */
        if (fragment != null) {
            ((BaseAlertDialog) fragment).dismissAllowingStateLoss();
        }

        if (bound) {
            if (service != null) {
                service.removeListener(serviceCallback);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                getActivity().unbindService(connection);
            }

            bound = false;
        }

        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (activity == null) {
            activity = (AppCompatActivity) getActivity();
        }

        adapter = new ViewPagerAdapter(activity.getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(6);
        viewPager.addOnPageChangeListener(viewPagerListener);
        int colorId = (childInActionMode ? R.color.action_mode : R.color.colorPrimary);
        setTabLayoutColor(colorId);
        tabLayout.setupWithViewPager(viewPager);

        if (savedInstanceState != null) {
            torrentId = savedInstanceState.getString(TAG_TORRENT_ID);
            isTorrentInfoChanged = savedInstanceState.getBoolean(TAG_TORRENT_INFO_CHANGED);
            isTorrentFilesChanged = savedInstanceState.getBoolean(TAG_TORRENT_FILES_CHANGED);
            childInActionMode = savedInstanceState.getBoolean(TAG_CHILD_IN_ACTION_MODE);
            currentFragPos = savedInstanceState.getInt(TAG_CURRENT_FRAG_POS);
        }

        if (isTorrentFilesChanged || isTorrentInfoChanged) {
            saveChangesButton.setVisibility(View.VISIBLE);
        } else {
            saveChangesButton.setVisibility(View.GONE);
        }

        saveChangesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveChangesButton.hide();

                if (isTorrentInfoChanged) {
                    applyInfoChanges();
                }

                if (isTorrentFilesChanged) {
                    applyFilesChanges();
                }

                isTorrentInfoChanged = false;
                isTorrentFilesChanged = false;
            }
        });

        if (savedInstanceState != null) {
            torrentId = savedInstanceState.getString(TAG_TORRENT_ID);
        }

        repo = new TorrentStorage(activity.getApplicationContext());
        if (torrentId != null) {
            torrent = repo.getTorrentByID(torrentId);
        }

        downloadingMetadata = torrent != null && torrent.isDownloadingMetadata();

        Intent torrentServiceIntent = new Intent(activity.getApplicationContext(), TorrentTaskService.class);
        activity.startService(torrentServiceIntent);

        activity.bindService(torrentServiceIntent, connection, Context.BIND_AUTO_CREATE);

        if (torrent != null) {
            updateTitle(torrent.getName());
        }
        activity.setSupportActionBar(toolbar);
        setHasOptionsMenu(true);

        if (activity.getSupportActionBar() != null) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    private void updateTitle(String name) {
        toolbar.setTitle(name);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(TAG_TORRENT_INFO_CHANGED, isTorrentInfoChanged);
        outState.putBoolean(TAG_TORRENT_FILES_CHANGED, isTorrentFilesChanged);
        outState.putString(TAG_TORRENT_ID, torrentId);
        outState.putBoolean(TAG_CHILD_IN_ACTION_MODE, childInActionMode);
        outState.putInt(TAG_CURRENT_FRAG_POS, currentFragPos);

        super.onSaveInstanceState(outState);
    }

    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            TorrentTaskService.LocalBinder binder = (TorrentTaskService.LocalBinder) service;
            DetailTorrentFragment.this.service = binder.getService();
            DetailTorrentFragment.this.service.addListener(serviceCallback);
            bound = true;
            initRequest();
            initFragments();
            startUpdateTorrentState();
        }

        public void onServiceDisconnected(ComponentName className) {
            DetailTorrentFragment.this.service.removeListener(serviceCallback);
            bound = false;
        }
    };

    public void setTorrentId(String id) {
        torrentId = id;
    }

    public String getTorrentId() {
        return torrentId;
    }

    private void initFragments() {
        if (!isAdded() || adapter.getCount() > 0)
            return;

        DetailTorrentInfoFragment fragmentInfo = DetailTorrentInfoFragment.newInstance(torrent, infoCache, stateCache);
        DetailTorrentStateFragment fragmentState = DetailTorrentStateFragment.newInstance(infoCache);
        DetailTorrentFilesFragment fragmentFiles = DetailTorrentFilesFragment.newInstance(getFileList(), getPrioritiesList());
        DetailTorrentTrackersFragment fragmentTrackers = DetailTorrentTrackersFragment.newInstance();
        DetailTorrentPeersFragment fragmentPeers = DetailTorrentPeersFragment.newInstance();
        int numPieces = infoCache != null ? infoCache.numPieces : 0;
        int pieceLength = infoCache != null ? infoCache.pieceLength : 0;
        DetailTorrentPiecesFragment fragmentPieces = DetailTorrentPiecesFragment.newInstance(numPieces, pieceLength);

        /* Removing previous ViewPagerAdapter fragments, if any */
        adapter.addFragment(fragmentInfo, INFO_FRAG_POS, getString(R.string.torrent_info));
        adapter.addFragment(fragmentState, STATE_FRAG_POS, getString(R.string.torrent_state));
        adapter.addFragment(fragmentFiles, FILE_FRAG_POS, getString(R.string.torrent_files));
        adapter.addFragment(fragmentTrackers, TRACKERS_FRAG_POS, getString(R.string.torrent_trackers));
        adapter.addFragment(fragmentPeers, PEERS_FRAG_POS, getString(R.string.torrent_peers));
        adapter.addFragment(fragmentPieces, PIECES_FRAG_POS, getString(R.string.torrent_pieces));

        adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(currentFragPos);
    }

    ViewPager.OnPageChangeListener viewPagerListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            /* Nothing */
        }

        @Override
        public void onPageSelected(int position) {
            currentFragPos = position;

            switch (position) {
                case STATE_FRAG_POS:
                    DetailTorrentStateFragment stateFrag =
                            (DetailTorrentStateFragment) adapter.getItem(STATE_FRAG_POS);
                    if (stateFrag != null) {
                        stateFrag.setState(stateCache);
                        stateFrag.setActiveAndSeedingTime(activeTimeCache, seedingTimeCache);
                    }
                    break;
                case FILE_FRAG_POS:
                    DetailTorrentFilesFragment fileFrag =
                            (DetailTorrentFilesFragment) adapter.getItem(FILE_FRAG_POS);
                    if (fileFrag != null) {
                        fileFrag.setFilesReceivedBytes(stateCache != null ? stateCache.filesReceivedBytes : null);
                    }
                    break;
                case TRACKERS_FRAG_POS:
                    DetailTorrentTrackersFragment trackersFrag =
                            (DetailTorrentTrackersFragment) adapter.getItem(TRACKERS_FRAG_POS);
                    if (trackersFrag != null) {
                        trackersFrag.setTrackersList(new ArrayList<>(trackersCache.getAll()));
                    }
                    break;
                case PEERS_FRAG_POS:
                    DetailTorrentPeersFragment peersFrag =
                            (DetailTorrentPeersFragment) adapter.getItem(PEERS_FRAG_POS);
                    if (peersFrag != null) {
                        peersFrag.setPeerList(new ArrayList<>(peersCache.getAll()));
                    }
                    break;
                case PIECES_FRAG_POS:
                    DetailTorrentPiecesFragment piecesFrag =
                            (DetailTorrentPiecesFragment) adapter.getItem(PIECES_FRAG_POS);

                    if (piecesFrag != null) {
                        piecesFrag.setPieces(piecesCache);
                        if (stateCache != null)
                            piecesFrag.setDownloadedPiecesCount(stateCache.downloadedPieces);
                    }
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            /* Nothing */
        }
    };

    private ArrayList<BencodeFileItem> getFileList() {
        return (infoCache != null ? infoCache.fileList : new ArrayList<>());
    }

    private List<Priority> getPrioritiesList() {
        return torrent != null ? torrent.getFilePriorities() : new ArrayList<>();
    }

    public void onTorrentInfoChanged() {
        isTorrentInfoChanged = true;

        saveChangesButton.show();
    }

    public void onTorrentInfoChangesUndone() {
        isTorrentInfoChanged = false;

        saveChangesButton.hide();
    }

    public void onTorrentFilesChanged() {
        isTorrentFilesChanged = true;

        saveChangesButton.show();
    }

    private void applyInfoChanges() {
        DetailTorrentInfoFragment infoFrag = (DetailTorrentInfoFragment) adapter.getItem(INFO_FRAG_POS);
        DetailTorrentFilesFragment fileFrag = (DetailTorrentFilesFragment) adapter.getItem(FILE_FRAG_POS);

        if (infoFrag == null || fileFrag == null) {
            return;
        }

        String name = infoFrag.getTorrentName();
        String path = infoFrag.getDownloadPath();
        boolean sequential = infoFrag.isSequentialDownload();

        if (FileIOUtils.getFreeSpace(path) < fileFrag.getSelectedFileSize()) {
            Snackbar snackbar = Snackbar.make(coordinatorLayout,
                    R.string.error_free_space,
                    Snackbar.LENGTH_LONG);
            snackbar.show();

            return;
        }

        if (service != null && torrent != null) {
            if (!name.equals(torrent.getName())) {
                service.setTorrentName(torrentId, name);
                torrent.setName(name);
                infoFrag.setTorrentName(name);
            }

            if (!path.equals(torrent.getDownloadPath())) {
                ArrayList<String> list = new ArrayList<>();
                list.add(torrentId);

                service.setTorrentDownloadPath(list, path);
                torrent.setDownloadPath(path);
                infoFrag.setDownloadPath(path);
            }

            if (sequential != torrent.isSequentialDownload()) {
                service.setSequentialDownload(torrentId, sequential);
                torrent.setSequentialDownload(sequential);
                infoFrag.setSequentialDownload(sequential);
            }
        }
    }

    private void applyFilesChanges() {
        DetailTorrentFilesFragment fileFrag = (DetailTorrentFilesFragment) adapter.getItem(FILE_FRAG_POS);

        if (fileFrag == null)
            return;

        Priority[] priorities = fileFrag.getPriorities();
        if (priorities != null) {
            fileFrag.disableSelectedFiles();
            changeFilesPriorityRequest(priorities);
        }
    }

    /*
     * Repaint TabLayout in ActionMode color if it created.
     */

    private void setTabLayoutColor(int colorId) {
        if (tabLayout == null) {
            return;
        }

        Utils.setBackground(tabLayout,
                ContextCompat.getDrawable(activity.getApplicationContext(), colorId));
    }

    public void onTrackersChanged(ArrayList<String> trackers, boolean replace) {
        trackersCache.clear();
        addTrackersRequest(trackers, replace);
    }

    public void showSnackbar(String text, int length) {
        Snackbar.make(coordinatorLayout,
                text,
                length)
                .show();
    }

    public void openFile(String relativePath) {
        if (relativePath == null) {
            return;
        }

        String path = torrent.getDownloadPath() + File.separator + relativePath;

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        /* Give user a choice than to open file (without determining MIME type) */
        intent.setDataAndType(Uri.fromFile(new File(path)), "*/*");
        if (this.isAdded()) {
            startActivity(Intent.createChooser(intent, getString(R.string.open_using)));
        }
    }

    private void torrentSaveChooseDialog() {
        Intent i = new Intent(activity, FileManagerDialog.class);

        FileManagerConfig config = new FileManagerConfig(FileIOUtils.getUserDirPath(),
                null,
                null,
                FileManagerConfig.DIR_CHOOSER_MODE);

        i.putExtra(FileManagerDialog.TAG_CONFIG, config);

        startActivityForResult(i, SAVE_TORRENT_FILE_CHOOSE_REQUEST);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.detail_torrent, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        MenuItem pauseResume = menu.findItem(R.id.pause_resume_torrent_menu);
        if (torrent == null || !torrent.isPaused()) {
            setButton(pauseResume, R.string.pause_torrent, R.drawable.ic_pause_white_24dp);
        } else {
            setButton(pauseResume, R.string.resume_torrent, R.drawable.ic_play_arrow);
        }

        pauseResume.setVisible(torrent == null || !torrent.isFinished());

        MenuItem saveTorrentFile = menu.findItem(R.id.save_torrent_file_menu);
        if (downloadingMetadata) {
            saveTorrentFile.setVisible(false);

        } else {
            saveTorrentFile.setVisible(true);
        }
    }

    private void setButton(MenuItem pauseResume, int titleId, int iconId) {
        pauseResume.setTitle(titleId);
        pauseResume.setIcon(iconId);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.pause_resume_torrent_menu:
                pauseResumeTorrentRequest();
                break;
            case R.id.delete_torrent_menu:
                if (getFragmentManager().findFragmentByTag(TAG_DELETE_TORRENT_DIALOG) == null) {
                    BaseAlertDialog deleteTorrentDialog = BaseAlertDialog.newInstance(
                            getString(R.string.deleting),
                            getString(R.string.delete_selected_torrent),
                            R.layout.dialog_delete_torrent,
                            getString(R.string.ok),
                            getString(R.string.cancel),
                            null,
                            DetailTorrentFragment.this);

                    deleteTorrentDialog.show(getFragmentManager(), TAG_DELETE_TORRENT_DIALOG);
                }
                break;
            case R.id.force_recheck_torrent_menu:
                forceRecheckRequest();
                break;
            case R.id.force_announce_torrent_menu:
                forceAnnounceRequest();
                break;
            case R.id.share_magnet_menu:
                getMagnetRequest();
                break;
            case R.id.save_torrent_file_menu:
                torrentSaveChooseDialog();
                break;
            case R.id.add_trackers_menu:
                if (getFragmentManager().findFragmentByTag(TAG_ADD_TRACKERS_DIALOG) == null) {
                    BaseAlertDialog addTrackersDialog = BaseAlertDialog.newInstance(
                            getString(R.string.add_trackers),
                            getString(R.string.dialog_add_trackers),
                            R.layout.dialog_multiline_text_input,
                            getString(R.string.add),
                            getString(R.string.replace),
                            getString(R.string.cancel),
                            DetailTorrentFragment.this);

                    addTrackersDialog.show(getFragmentManager(), TAG_ADD_TRACKERS_DIALOG);
                }
                break;
            case R.id.torrent_speed_limit:
                if (getFragmentManager().findFragmentByTag(TAG_SPEED_LIMIT_DIALOG) == null) {
                    BaseAlertDialog speedLimitDialog = BaseAlertDialog.newInstance(
                            getString(R.string.speed_limit_title),
                            getString(R.string.speed_limit_dialog),
                            R.layout.dialog_speed_limit,
                            getString(R.string.ok),
                            getString(R.string.cancel),
                            null,
                            DetailTorrentFragment.this);

                    speedLimitDialog.show(getFragmentManager(), TAG_SPEED_LIMIT_DIALOG);
                }
                break;
        }

        return true;
    }

    @Override
    public void onShow(final AlertDialog dialog) {
        if (dialog == null) {
            return;
        }

        if (getFragmentManager().findFragmentByTag(TAG_ADD_TRACKERS_DIALOG) != null) {
            final TextInputEditText field =
                    dialog.findViewById(R.id.multiline_text_input_dialog);
            final TextInputLayout fieldLayout =
                    dialog.findViewById(R.id.layout_multiline_text_input_dialog);

            /* Dismiss error label if user has changed the text */
            if (field != null && fieldLayout != null) {
                field.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        /* Nothing */
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        fieldLayout.setErrorEnabled(false);
                        fieldLayout.setError(null);

                        /* Clear selection of invalid url */
                        Spannable text = field.getText();
                        ForegroundColorSpan[] errorSpans = text.getSpans(0, text.length(),
                                ForegroundColorSpan.class);
                        for (ForegroundColorSpan span : errorSpans) {
                            text.removeSpan(span);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        /* Nothing */
                    }
                });
            }

            /*
             * It is necessary in order to the dialog is not closed by
             * pressing add/replace button if the text checker gave a false result
             */
            Button addButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            Button replaceButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);

            addButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (field != null && fieldLayout != null) {
                        String text = field.getText().toString();
                        List<String> urls = Arrays.asList(text.split(Utils.getLineSeparator()));

                        if (checkEditTextField(urls, fieldLayout, field)) {
                            addTrackersRequest(new ArrayList<>(urls), false);

                            dialog.dismiss();
                        }
                    }
                }
            });

            replaceButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (field != null && fieldLayout != null) {
                        String text = field.getText().toString();
                        List<String> urls = Arrays.asList(text.split(Utils.getLineSeparator()));

                        if (checkEditTextField(urls, fieldLayout, field)) {
                            addTrackersRequest(new ArrayList<>(urls), true);

                            dialog.dismiss();
                        }
                    }
                }
            });

            /* Inserting links from the clipboard */
            String clipboard = Utils.getClipboard(activity.getApplicationContext());

            if (clipboard != null && field != null) {
                List<String> urls = Arrays.asList(clipboard.split(Utils.getLineSeparator()));
                ArrayList<String> validUrls = new ArrayList<>();

                for (String url : urls) {
                    if (Utils.isValidTrackerUrl(url)) {
                        validUrls.add(url);
                    }
                }

                field.setText(TextUtils.join(Utils.getLineSeparator(), validUrls));
            }

        } else if (getFragmentManager().findFragmentByTag(TAG_SPEED_LIMIT_DIALOG) != null) {
            TextInputEditText upload = dialog.findViewById(R.id.upload_limit);
            TextInputEditText download = dialog.findViewById(R.id.download_limit);

            if (upload != null && download != null) {
                int minSpeedLimit = 0;
                int maxSpeedLimit = Integer.MAX_VALUE;
                InputFilter[] filter = new InputFilter[]{new InputFilterMinMax(minSpeedLimit, maxSpeedLimit)};

                upload.setFilters(filter);
                if (TextUtils.isEmpty(upload.getText())) {
                    upload.setText((uploadSpeedLimit != -1 ?
                            Integer.toString(uploadSpeedLimit / 1024) : Integer.toString(minSpeedLimit)));
                }

                download.setFilters(filter);
                if (TextUtils.isEmpty(download.getText())) {
                    download.setText((downloadSpeedLimit != -1 ?
                            Integer.toString(downloadSpeedLimit / 1024) : Integer.toString(minSpeedLimit)));
                }
            }
        }
    }

    @Override
    public void onPositiveClicked(@Nullable View v) {
        if (v != null) {
            if (getFragmentManager().findFragmentByTag(TAG_DELETE_TORRENT_DIALOG) != null) {
                CheckBox withFiles = v.findViewById(R.id.dialog_delete_torrent_with_downloaded_files);
                deleteTorrentRequest(withFiles.isChecked());

                finish(new Intent(), FragmentCallback.ResultCode.CANCEL);

            } else if (getFragmentManager().findFragmentByTag(TAG_SPEED_LIMIT_DIALOG) != null) {
                TextInputEditText upload = v.findViewById(R.id.upload_limit);
                TextInputEditText download = v.findViewById(R.id.download_limit);

                if (!TextUtils.isEmpty(upload.getText().toString()) && !TextUtils.isEmpty(download.getText().toString())) {
                    uploadSpeedLimit = Integer.parseInt(upload.getText().toString()) * 1024;
                    downloadSpeedLimit = Integer.parseInt(download.getText().toString()) * 1024;

                    setSpeedLimitRequest(uploadSpeedLimit, downloadSpeedLimit);
                }

            }
        }
    }

    @Override
    public void onNegativeClicked(@Nullable View v) {
        /* Nothing */
    }

    @Override
    public void onNeutralClicked(@Nullable View v) {
        /* Nothing */
    }

    private boolean checkEditTextField(List<String> strings,
                                       TextInputLayout layout,
                                       TextInputEditText field) {
        if (strings == null || layout == null) {
            return false;
        }

        if (strings.isEmpty()) {
            layout.setErrorEnabled(true);
            layout.setError(getString(R.string.error_empty_link));
            layout.requestFocus();

            return false;
        }

        boolean valid = true;
        int curLineStartIndex = 0;
        for (String s : strings) {
            if (!Utils.isValidTrackerUrl(s)) {
                /* Select invalid url */
                field.getText().setSpan(new ForegroundColorSpan(
                                ContextCompat.getColor(activity.getApplicationContext(), R.color.error)),
                        curLineStartIndex,
                        curLineStartIndex + s.length(),
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                valid = false;
            }
            /* Considering newline char */
            curLineStartIndex += s.length() + 1;
        }

        if (valid) {
            layout.setErrorEnabled(false);
            layout.setError(null);
        } else {
            layout.setErrorEnabled(true);
            layout.setError(getString(R.string.error_invalid_link));
            layout.requestFocus();
        }

        return valid;
    }

    private void saveErrorTorrentFileDialog(Exception e) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, getString(R.string.error_save_torrent_file), Toast.LENGTH_SHORT).show();
        }
        Crashlytics.logException(e);
        Timber.e(e);
    }

    private void initRequest() {
        if (!bound || service == null) {
            return;
        }

        infoCache = service.getTorrentMetaInfo(torrentId);
        uploadSpeedLimit = service.getUploadSpeedLimit(torrentId);
        downloadSpeedLimit = service.getDownloadSpeedLimit(torrentId);
    }

    private void changeFilesPriorityRequest(Priority[] priorities) {
        if (!bound || service == null || priorities == null || torrentId == null) {
            return;
        }

        service.changeFilesPriority(torrentId, priorities);
    }

    private void addTrackersRequest(ArrayList<String> trackers, boolean replace) {
        if (!bound || service == null || trackers == null || torrentId == null) {
            return;
        }

        if (replace) {
            service.replaceTrackers(torrentId, trackers);

        } else {
            service.addTrackers(torrentId, trackers);
        }
    }

    private void deleteTorrentRequest(boolean withFiles) {
        if (!bound || service == null) {
            return;
        }

        ArrayList<String> list = new ArrayList<>();
        list.add(torrentId);

        service.deleteTorrents(list, withFiles);
    }

    private void forceRecheckRequest() {
        if (!bound || service == null) {
            return;
        }

        ArrayList<String> list = new ArrayList<>();
        list.add(torrentId);

        service.forceRecheckTorrents(list);
    }

    private void forceAnnounceRequest() {
        if (!bound || service == null) {
            return;
        }

        ArrayList<String> list = new ArrayList<>();
        list.add(torrentId);

        service.forceAnnounceTorrents(list);
    }

    public void pauseResumeTorrentRequest() {
        if (!bound || service == null) {
            return;
        }

        try {
            service.pauseResumeTorrent(torrentId);
        } catch (WifiOnlyException e) {
            Context context = getContext();
            if (context != null) {
                Toast.makeText(context, context.getString(R.string.you_cant_resume_without_wifi), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getMagnetRequest() {
        if (!bound || service == null) {
            return;
        }

        String magnet = service.getMagnet(torrentId);

        if (magnet != null) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "magnet");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, magnet);
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_via)));
        }
    }

    private void setSpeedLimitRequest(int uploadSpeedLimit, int downloadSpeedLimit) {
        if (!bound || service == null) {
            return;
        }

        service.setUploadSpeedLimit(torrentId, uploadSpeedLimit);
        service.setDownloadSpeedLimit(torrentId, downloadSpeedLimit);
    }

    private void startUpdateTorrentState() {
        if (!bound) {
            return;
        }

        updateTorrentStateHandler.post(updateTorrentState);
    }

    private void stopUpdateTorrentState() {
        updateTorrentStateHandler.removeCallbacks(updateTorrentState);
    }

    private void getActiveAndSeedingTimeRequest() {
        long activeTime = service.getActiveTime(torrentId);
        long seedingTime = service.getSeedingTime(torrentId);

        activeTimeCache = activeTime;
        seedingTimeCache = seedingTime;

        if (viewPager != null && viewPager.getCurrentItem() == STATE_FRAG_POS) {
            DetailTorrentStateFragment stateFrag =
                    (DetailTorrentStateFragment) adapter.getItem(STATE_FRAG_POS);

            if (stateFrag != null) {
                stateFrag.setActiveAndSeedingTime(activeTime, seedingTime);
            }
        }
    }

    private void getTrackersStatesRequest() {
        ArrayList<TrackerStateParcel> states = service.getTrackerStatesList(torrentId);

        if (states == null) {
            return;
        }

        if (!trackersCache.containsAll(states)) {
            trackersCache.clear();
            trackersCache.putAll(states);

            if (viewPager.getCurrentItem() == TRACKERS_FRAG_POS) {
                DetailTorrentTrackersFragment trackersFrag =
                        (DetailTorrentTrackersFragment) adapter.getItem(TRACKERS_FRAG_POS);

                if (trackersFrag != null) {
                    trackersFrag.setTrackersList(states);
                }
            }
        }
    }

    private void getPeerStatesRequest() {
        ArrayList<PeerStateParcel> states = service.getPeerStatesList(torrentId);

        if (states == null) {
            return;
        }

        if (!peersCache.containsAll(states) || states.isEmpty()) {
            peersCache.clear();
            peersCache.putAll(states);

            if (viewPager.getCurrentItem() == PEERS_FRAG_POS) {
                DetailTorrentPeersFragment peersFrag =
                        (DetailTorrentPeersFragment) adapter.getItem(PEERS_FRAG_POS);

                if (peersFrag != null) {
                    peersFrag.setPeerList(states);
                }
            }
        }
    }

    private void getPiecesRequest() {
        boolean[] pieces = service.getPieces(torrentId);

        if (pieces == null) {
            return;
        }

        if (!Arrays.equals(piecesCache, pieces)) {
            piecesCache = pieces;

            if (viewPager.getCurrentItem() == PIECES_FRAG_POS) {
                DetailTorrentPiecesFragment piecesFrag =
                        (DetailTorrentPiecesFragment) adapter.getItem(PIECES_FRAG_POS);

                if (piecesFrag != null) {
                    piecesFrag.setPieces(pieces);
                }
            }
        }
    }

    TorrentServiceCallback serviceCallback = new TorrentServiceCallback() {
        @Override
        public void onTorrentStateChanged(TorrentStateParcel state) {
            if (state != null && state.torrentId.equals(torrentId)) {
                handleTorrentState(state);
            }
        }

        @Override
        public void onTorrentAdded(TorrentStateParcel state) {
            /* Nothing */
        }

        @Override
        public void onTorrentsStateChanged(Bundle states) {
            if (states == null || !states.containsKey(torrentId)) {
                return;
            }

            handleTorrentState((TorrentStateParcel) states.getParcelable(torrentId));
        }

        @Override
        public void onTorrentRemoved(TorrentStateParcel state) {
            /* Nothing */
        }
    };

    private void handleTorrentState(final TorrentStateParcel state) {
        if (state == null) {
            return;
        }

        activity.runOnUiThread(() -> {
            stateCache = state;
            DetailTorrentInfoFragment infoFrag = (DetailTorrentInfoFragment) adapter.getItem(INFO_FRAG_POS);

            if (infoFrag != null) {
                infoFrag.setState(state);
            }

            if (downloadingMetadata && stateCache.stateCode != TorrentStateCode.DOWNLOADING_METADATA) {
                downloadingMetadata = false;

                if (torrentId != null) {
                    torrent = repo.getTorrentByID(torrentId);
                }

                TorrentMetaInfo info = service.getTorrentMetaInfo(torrentId);

                if (info != null && (infoCache == null || !infoCache.equals(info))) {
                    infoCache = info;


                    if (infoFrag != null) {
                        infoFrag.setInfo(infoCache);
                    }
                    DetailTorrentStateFragment stateFrag =
                            (DetailTorrentStateFragment) adapter.getItem(STATE_FRAG_POS);

                    if (stateFrag != null) {
                        stateFrag.setInfo(infoCache);
                    }
                    DetailTorrentFilesFragment fileFrag =
                            (DetailTorrentFilesFragment) adapter.getItem(FILE_FRAG_POS);

                    if (fileFrag != null) {
                        fileFrag.setFilesAndPriorities(getFileList(), getPrioritiesList());
                    }
                    DetailTorrentPiecesFragment piecesFrag =
                            (DetailTorrentPiecesFragment) adapter.getItem(PIECES_FRAG_POS);

                    if (piecesFrag != null) {
                        piecesFrag.setPiecesCountAndSize(infoCache.numPieces, infoCache.pieceLength);
                    }
                }

                activity.invalidateOptionsMenu();
            }

            if (torrent != null) {
                if (state.stateCode == TorrentStateCode.PAUSED || torrent.isPaused()) {
                    torrent.setPaused(state.stateCode == TorrentStateCode.PAUSED);

                    /* Redraw pause/resume menu */

                    activity.invalidateOptionsMenu();

                }

                if (toolbar != null && state.name != null) {
                    String oldName = toolbar.getTitle().toString();
                    if (!oldName.equals(state.name)) {
                        updateTitle(state.name);
                    }
                }
            }

            if (infoFrag != null) {
                infoFrag.setState(state);
            }

            if (viewPager != null) {
                switch (viewPager.getCurrentItem()) {
                    case INFO_FRAG_POS:
                        if (infoFrag != null && state.name != null) {
                            String oldName = infoFrag.getTorrentName();
                            if (!oldName.equals(state.name) && oldName.equals(state.torrentId)) {
                                infoFrag.updateTorrentName(state.name);
                            }
                        }

                        break;
                    case STATE_FRAG_POS:
                        DetailTorrentStateFragment stateFrag =
                                (DetailTorrentStateFragment) adapter.getItem(STATE_FRAG_POS);

                        if (stateFrag != null) {
                            stateFrag.setState(state);
                        }

                        break;
                    case FILE_FRAG_POS:
                        DetailTorrentFilesFragment fileFrag =
                                (DetailTorrentFilesFragment) adapter.getItem(FILE_FRAG_POS);

                        if (fileFrag != null) {
                            fileFrag.setFilesReceivedBytes(state.filesReceivedBytes);
                        }

                        break;
                    case PIECES_FRAG_POS:
                        DetailTorrentPiecesFragment piecesFrag =
                                (DetailTorrentPiecesFragment) adapter.getItem(PIECES_FRAG_POS);

                        if (piecesFrag != null) {
                            piecesFrag.setDownloadedPiecesCount(state.downloadedPieces);
                        }

                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SAVE_TORRENT_FILE_CHOOSE_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data.hasExtra(FileManagerDialog.TAG_RETURNED_PATH)) {
                String path = data.getStringExtra(FileManagerDialog.TAG_RETURNED_PATH);

                if (path != null && torrentId != null && torrent != null) {
                    try {
                        if (TorrentUtils.copyTorrentFile(activity.getApplicationContext(),
                                torrentId, path, torrent.getName() + ".torrent")) {
                            showSnackbar(getString(R.string.save_torrent_file_successfully),
                                    Snackbar.LENGTH_SHORT);
                        } else {
                            saveErrorTorrentFileDialog(null);
                        }

                    } catch (IOException e) {
                        saveErrorTorrentFileDialog(e);
                    }
                }
            }
        }
    }

    public void onBackPressed() {
        finish(new Intent(), FragmentCallback.ResultCode.BACK);
    }

    private void finish(Intent intent, FragmentCallback.ResultCode code) {
        ((FragmentCallback) activity).fragmentFinished(intent, code);
    }
}
