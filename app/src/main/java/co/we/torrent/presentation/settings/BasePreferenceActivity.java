package co.we.torrent.presentation.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

import co.we.torrent.R;
import co.we.torrent.other.Utils;

public class BasePreferenceActivity extends AppCompatActivity {
    public static final String TAG_CONFIG = "config";

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.act_settings);

        String fragment = null;
        String title = null;

        Intent intent = getIntent();
        if (intent.hasExtra(TAG_CONFIG)) {
            PreferenceActivityConfig config = intent.getParcelableExtra(TAG_CONFIG);
            fragment = config.getFragment();
            title = config.getTitle();
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            if (title != null) {
                toolbar.setTitle(title);
            }
            setSupportActionBar(toolbar);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (fragment != null && savedInstanceState == null) {
            setFragment(getFragment(fragment));
        }
    }

    public <F extends PreferenceFragmentCompat> void setFragment(F fragment) {
        if (fragment == null) {
            return;
        }

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment).commit();
    }

    private <F extends PreferenceFragmentCompat> F getFragment(String fragment) {
        if (fragment != null) {
            if (fragment.equals(BehaviorSettingsFragment.class.getSimpleName())) {
                return (F) BehaviorSettingsFragment.newInstance();
            } else if (fragment.equals(StorageSettingsFragment.class.getSimpleName())) {
                return (F) StorageSettingsFragment.newInstance();
            } else if (fragment.equals(LimitationsSettingsFragment.class.getSimpleName())) {
                return (F) LimitationsSettingsFragment.newInstance();
            } else if (fragment.equals(NetworkSettingsFragment.class.getSimpleName())) {
                return (F) NetworkSettingsFragment.newInstance();
            } else if (fragment.equals(ProxySettingsFragment.class.getSimpleName())) {
                return (F) ProxySettingsFragment.newInstance();
            } else {
                return null;
            }
        }

        return null;
    }

    public void setTitle(String title) {
        if (toolbar != null) {
            toolbar.setTitle(title);
        }
    }

    public String getToolbarTitle() {
        return (toolbar != null ? toolbar.getTitle().toString() : null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }
}
