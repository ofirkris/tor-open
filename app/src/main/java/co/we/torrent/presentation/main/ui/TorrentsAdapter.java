package co.we.torrent.presentation.main.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import co.we.torrent.R;
import co.we.torrent.data.core.TorrentStateCode;
import co.we.torrent.data.core.sorting.TorrentSortingComparator;
import co.we.torrent.data.core.stateparcel.TorrentStateParcel;
import co.we.torrent.other.TimeUtils;
import co.we.torrent.other.Utils;
import co.we.torrent.presentation.custom_views.CircleProgress;
import co.we.torrent.presentation.old.adapters.SelectableAdapter;

import static co.we.torrent.models.TorrentListType.ALL;
import static co.we.torrent.models.TorrentListType.FINISHED;
import static co.we.torrent.models.TorrentListType.QUEUED;

public class TorrentsAdapter extends SelectableAdapter<TorrentsAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        @BindView(R.id.frame) ViewGroup frame;
        @BindView(R.id.item_title) TextView name;
        @BindView(R.id.item_btn_play_pause) ImageButton pauseButton;
        @BindView(R.id.item_progress_text) TextView progressText;
        @BindView(R.id.item_progress) CircleProgress progressCircleProgress;
        @BindView(R.id.item_size) TextView downloadCounter;
        @BindView(R.id.item_speed) TextView downloadUploadSpeed;
        @BindView(R.id.item_time) TextView time;
        @BindView(R.id.item_watch) ImageView timeIcon;
        @BindView(R.id.item_btn_info) ImageView btnInfo;
        @BindView(R.id.item_state) TextView state;
        @BindString(R.string.second) String second;

        private Context context;
        private ClickListener listener;
        private List<TorrentStateParcel> states;
        private AnimatedVectorDrawableCompat playToPauseAnim;
        private AnimatedVectorDrawableCompat pauseToPlayAnim;
        private AnimatedVectorDrawableCompat currAnim;

        public ViewHolder(View itemView, final ClickListener listener, final List<TorrentStateParcel> states) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.context = itemView.getContext();
            this.listener = listener;
            this.states = states;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            playToPauseAnim = AnimatedVectorDrawableCompat.create(context, R.drawable.play_to_pause);
            pauseToPlayAnim = AnimatedVectorDrawableCompat.create(context, R.drawable.pause_to_play);

            pauseButton.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position >= 0) {
                    TorrentStateParcel state = states.get(position);
                    listener.onPauseButtonClicked(position, state);
                }
            });

            btnInfo.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position >= 0) {
                    TorrentStateParcel state = states.get(position);
                    listener.onInfoClicked(position, state);
                }
            });
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            if (listener != null && position >= 0) {
                TorrentStateParcel state = states.get(position);
                listener.onItemClicked(position, state);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            int position = getAdapterPosition();

            if (listener != null && position >= 0) {
                TorrentStateParcel state = states.get(getAdapterPosition());
                listener.onItemLongClicked(position, state);

                return true;
            }

            return false;
        }

        void setPauseButtonState(boolean isPause) {
            AnimatedVectorDrawableCompat prevAnim = currAnim;
            currAnim = (isPause ? pauseToPlayAnim : playToPauseAnim);
            pauseButton.setImageDrawable(currAnim);
            if (currAnim != prevAnim)
                currAnim.start();
        }

        public interface ClickListener {
            void onItemClicked(int position, TorrentStateParcel torrentState);

            void onItemLongClicked(int position, TorrentStateParcel torrentState);

            void onPauseButtonClicked(int position, TorrentStateParcel torrentState);

            void onInfoClicked(int position, TorrentStateParcel torrentState);
        }
    }

    private Context context;
    private ViewHolder.ClickListener clickListener;
    private List<TorrentStateParcel> currentItems = new ArrayList<>();
    private List<TorrentStateParcel> allItems = new ArrayList<>();
    private ArrayList<String> selectedList = new ArrayList<>();
    private AtomicReference<TorrentStateParcel> curOpenTorrent = new AtomicReference<>();
    private final DisplayFilter displayFilter;
    private TorrentSortingComparator sorting;

    public TorrentsAdapter(Context context, DisplayFilter displayFilter, ViewHolder.ClickListener clickListener,
                           TorrentSortingComparator sorting) {
        this.context = context;
        this.displayFilter = displayFilter;
        this.clickListener = clickListener;
        this.sorting = sorting;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_torrent, parent, false);

        return new ViewHolder(v, clickListener, currentItems);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TorrentStateParcel state = currentItems.get(position);

        TypedArray a = context.obtainStyledAttributes(new TypedValue().data, new int[]{
                R.attr.defaultSelectRect,
                R.attr.defaultRectRipple
        });

        if (isSelected(position)) {
            Utils.setBackground(holder.frame, a.getDrawable(0));
        } else {
            Utils.setBackground(holder.frame, a.getDrawable(1));
        }
        a.recycle();

        holder.name.setText(state.name);

        String totalBytes = Formatter.formatShortFileSize(context, state.totalBytes);
        String receivedBytes;
        if (state.progress == 100) {
            receivedBytes = totalBytes;
        } else {
            receivedBytes = Formatter.formatShortFileSize(context, (long) (((double) state.totalBytes / 100) * state.progress));
        }

        holder.progressText.setText(String.valueOf(state.progress));
        holder.progressCircleProgress.setProgress(state.progress);
        String bytes = receivedBytes + "/" + totalBytes;
        holder.downloadCounter.setText(bytes);


        String downloadSpeed = Formatter.formatShortFileSize(context, state.downloadSpeed);
        String uploadSpeed = Formatter.formatShortFileSize(context, state.uploadSpeed);
        String speed = Utils.ARROW_DOWN + downloadSpeed + "/s | " + uploadSpeed + "/s" + Utils.ARROW_UP;

        holder.downloadUploadSpeed.setText(speed);

        String stateString = getState(state);
        holder.state.setText(stateString);

        if (getButtonsVisibility(state, state.progress)) {
            holder.pauseButton.setVisibility(View.VISIBLE);
            holder.btnInfo.setVisibility(View.INVISIBLE);
        } else {
            holder.pauseButton.setVisibility(View.INVISIBLE);
            holder.btnInfo.setVisibility(View.VISIBLE);
        }

        String ETA;
        if (state.ETA == -1) {
            ETA = Utils.INFINITY_SYMBOL;
            holder.timeIcon.setVisibility(View.INVISIBLE);
        } else if (state.ETA == 0) {
            ETA = Utils.THREE_DOTS;
            holder.timeIcon.setVisibility(View.INVISIBLE);
        } else {
            ETA = TimeUtils.formatTime(state.ETA);
            holder.timeIcon.setVisibility(View.VISIBLE);
        }
        holder.time.setText(ETA);
        holder.setPauseButtonState(state.stateCode == TorrentStateCode.PAUSED);

        TorrentStateParcel curTorrent = curOpenTorrent.get();
        if (curTorrent != null) {
            curTorrent.setEqualsById(true);
        }
    }

    private String getState(TorrentStateParcel state) {
        String stateString;
        switch (state.stateCode) {
            case DOWNLOADING:
                if (state.seeds > 0) {
                    stateString = context.getString(R.string.torrent_status_downloading);
                } else {
                    stateString = context.getString(R.string.torrent_status_search_seeds);
                }
                break;
            case SEEDING:
                stateString = context.getString(R.string.torrent_status_seeding);
                break;
            case PAUSED:
                stateString = context.getString(R.string.torrent_status_paused);
                break;
            case STOPPED:
                stateString = context.getString(R.string.torrent_status_stopped);
                break;
            case FINISHED:
                stateString = context.getString(R.string.torrent_status_finished);
                break;
            case CHECKING:
                stateString = context.getString(R.string.torrent_status_checking);
                break;
            case DOWNLOADING_METADATA:
                stateString = context.getString(R.string.torrent_status_downloading_metadata);
                break;
            default:
                stateString = "";
        }

        return stateString;
    }

    private boolean getButtonsVisibility(TorrentStateParcel state, int progress) {
        switch (state.stateCode) {
            default:
            case PAUSED:
            case STOPPED:
                return progress != 100;
            case DOWNLOADING:
            case DOWNLOADING_METADATA:
                return true;
            case SEEDING:
            case FINISHED:
            case CHECKING:
                return false;
        }
    }

    private void setEqualsMethod(Collection<TorrentStateParcel> states) {
        for (TorrentStateParcel state : states) {
            if (state == null) {
                continue;
            }
            state.setEqualsById(true);
        }
    }

    public synchronized void addItems(Collection<TorrentStateParcel> states) {
        if (states == null) {
            return;
        }

        setEqualsMethod(states);
        List<TorrentStateParcel> statesList = displayFilter.filter(states);
        currentItems.addAll(statesList);
        Collections.sort(currentItems, sorting);

        notifyItemRangeInserted(0, statesList.size());

        allItems.addAll(states);
    }


    public synchronized void updateItem(TorrentStateParcel torrentState) {
        if (torrentState == null) {
            return;
        }

        torrentState.setEqualsById(true);

        if (!currentItems.contains(torrentState)) {
            TorrentStateParcel state = displayFilter.filter(torrentState);

            if (state != null) {
                currentItems.add(torrentState);
                Collections.sort(currentItems, sorting);

                notifyItemInserted(currentItems.indexOf(state));
            }

        } else {
            int position = currentItems.indexOf(torrentState);

            if (position >= 0) {
                currentItems.remove(position);
                TorrentStateParcel state = displayFilter.filter(torrentState);

                if (state != null) {
                    currentItems.add(position, torrentState);
                    Collections.sort(currentItems, sorting);
                    notifyItemChanged(position);
                } else {
                    notifyItemRemoved(position);
                    if (selectedList.contains(torrentState.torrentId)) {
                        selectedList.remove(torrentState.torrentId);
                    }
                }
            }
        }

        if (!allItems.contains(torrentState)) {
            allItems.add(torrentState);

            return;
        }

        int position = allItems.indexOf(torrentState);

        if (position < 0) {
            return;
        }

        allItems.remove(position);
        allItems.add(position, torrentState);
    }

    public void clearAll() {
        allItems.clear();

        int size = currentItems.size();
        if (size > 0) {
            currentItems.clear();

            notifyItemRangeRemoved(0, size);
        }
        selectedList.clear();
    }

    public void deleteItem(TorrentStateParcel state) {
        if (state == null) {
            return;
        }

        state.setEqualsById(true);
        currentItems.remove(state);
        allItems.remove(state);
        if (selectedList.contains(state.torrentId)) {
            selectedList.remove(state.torrentId);
        }

        notifyDataSetChanged();
    }

    public void setSorting(TorrentSortingComparator sorting) {
        if (sorting == null) {
            return;
        }

        this.sorting = sorting;
        Collections.sort(currentItems, sorting);

        notifyItemRangeChanged(0, currentItems.size());
    }

    public TorrentStateParcel getItem(int position) {
        if (position < 0 || position >= currentItems.size()) {
            return null;
        }

        return currentItems.get(position);
    }

    public int getItemPosition(TorrentStateParcel state) {
        if (state == null) {
            return -1;
        }

        state.setEqualsById(true);

        return currentItems.indexOf(state);
    }

    public boolean isEmpty() {
        return currentItems.isEmpty();
    }

    @Override
    public int getItemCount() {
        return currentItems.size();
    }

    public int getSelectedCount() {
        return selectedList.size();
    }

    public void clearSelectedList() {
        selectedList = new ArrayList<>();
        this.clearSelection();
    }

    public ArrayList<String> getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(List<String> list) {
        this.selectedList = new ArrayList<>(list);
    }

    public void toggleSelect(String id, int position) {
        this.toggleSelection(position);
        if (selectedList.contains(id)) {
            selectedList.remove(id);
        } else {
            selectedList.add(id);
        }

        notifyItemChanged(position);
    }

    public void selectAll() {
        if (selectedList.size() == currentItems.size()) {
            selectedList = new ArrayList<>();
        } else {
            for (TorrentStateParcel item : this.currentItems) {
                if (!selectedList.contains(item.torrentId)) {
                    selectedList.add(item.torrentId);
                }
            }
        }

        notifyDataSetChanged();
    }

    public static class DisplayFilter {
        public static TorrentsAdapter.DisplayFilter create(int type) {
            switch (type) {
                default:
                case ALL:
                    return new TorrentsAdapter.DisplayFilter(TorrentStateCode.ALL_GROUP);
                case QUEUED:
                    return new TorrentsAdapter.DisplayFilter(TorrentStateCode.QUEUED_GROUP);
                case FINISHED:
                    return new TorrentsAdapter.DisplayFilter(TorrentStateCode.FINISHED_GROUP);

            }
        }

        private TorrentStateCode constraintCode;

        private DisplayFilter(TorrentStateCode constraint) {
            constraintCode = constraint;
        }

        public List<TorrentStateParcel> filter(Collection<TorrentStateParcel> states) {
            List<TorrentStateParcel> filtered = new ArrayList<>();

            if (states != null) {
                if (constraintCode != null) {
                    for (TorrentStateParcel state : states) {
                        if (isItAppropriate(state.stateCode, state.progress)) {
                            filtered.add(state);
                        }
                    }
                } else {
                    filtered.addAll(states);
                }
            }

            return filtered;
        }

        public TorrentStateParcel filter(TorrentStateParcel state) {
            if (state == null) {
                return null;
            }

            if (constraintCode != null) {
                if (isItAppropriate(state.stateCode, state.progress)) {
                    return state;
                }

            } else {
                return state;
            }

            return null;
        }

        private boolean isItAppropriate(TorrentStateCode stateCode, int progress) {
            switch (constraintCode) {
                case ALL_GROUP:
                    return true;
                case QUEUED_GROUP:
                    return stateCode == TorrentStateCode.DOWNLOADING
                            || (stateCode == TorrentStateCode.CHECKING && progress < 100)
                            || (stateCode == TorrentStateCode.PAUSED && progress < 100)
                            || stateCode == TorrentStateCode.DOWNLOADING_METADATA;
                case FINISHED_GROUP:
                    return stateCode == TorrentStateCode.FINISHED
                            || (stateCode == TorrentStateCode.CHECKING && progress == 100)
                            || (stateCode == TorrentStateCode.PAUSED && progress == 100)
                            || stateCode == TorrentStateCode.STOPPED
                            || stateCode == TorrentStateCode.SEEDING;
                default:
                    return false;
            }
        }
    }
}
