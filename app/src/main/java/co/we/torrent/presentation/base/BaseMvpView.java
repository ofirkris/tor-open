package co.we.torrent.presentation.base;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Alexander Artemov
 */
public interface BaseMvpView extends MvpView {
    @StateStrategyType(OneExecutionStateStrategy.class)
    void toggleLoading(boolean visible, int messageId);
}
