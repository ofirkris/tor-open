package co.we.torrent.presentation.base;

import android.app.ProgressDialog;
import android.os.Handler;

import com.arellomobile.mvp.MvpAppCompatFragment;

/**
 * @author Alexander Artemov
 */
public class BaseFragment extends MvpAppCompatFragment implements BaseMvpView {

    protected ProgressDialog loading;

    @Override
    public void toggleLoading(boolean visible, int messageId) {
        clearLoadingDialog();
        if (visible) {
            loading = ProgressDialog.show(getActivity(), "",
                    getString(messageId), true);
            new Handler().postDelayed(this::clearLoadingDialog, 10000);
        }
    }

    protected void clearLoadingDialog() {
        if (loading != null && loading.isShowing()) {
            loading.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        clearLoadingDialog();
    }
}
