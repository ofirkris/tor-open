package co.we.torrent.presentation.old.fragments.details;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.we.torrent.R;
import co.we.torrent.data.core.Torrent;
import co.we.torrent.data.core.TorrentMetaInfo;
import co.we.torrent.data.core.TorrentStateCode;
import co.we.torrent.data.core.stateparcel.TorrentStateParcel;
import co.we.torrent.other.ButterknifeUtils;
import co.we.torrent.other.FileIOUtils;
import co.we.torrent.other.TorrentUtils;
import co.we.torrent.presentation.old.dialogs.filemanager.FileManagerConfig;
import co.we.torrent.presentation.old.dialogs.filemanager.FileManagerDialog;
import co.we.torrent.presentation.old.fragments.DetailTorrentFragment;

/*
 * The fragment for displaying torrent metainformation,
 * taken from bencode. Part of DetailTorrentFragment.
 */

public class DetailTorrentInfoFragment extends Fragment {

    private static final String TAG_INFO = "info";
    private static final String TAG_TORRENT = "torrent";
    private static final String TAG_DOWNLOAD_DIR = "download_dir";
    private static final String TAG_NAME = "name";
    private static final String TAG_IS_SEQUENTIAL = "is_sequential";

    private static final int DIR_CHOOSER_REQUEST = 1;

    private AppCompatActivity activity;
    private DetailTorrentFragment.Callback callback;
    private TorrentMetaInfo info;
    private TorrentStateParcel state;
    private Torrent torrent;
    private String downloadDir = "";
    private String name = "";
    private boolean isSequentialDownload = false;
    private boolean wasChangedManually = false;

    @BindView(R.id.name_field) EditText torrentNameField;
    @BindView(R.id.name_layout) TextInputLayout layoutTorrentName;
    @BindView(R.id.hash_field) TextView sha1HashView;
    @BindView(R.id.comment_field) TextView commentView;
    @BindView(R.id.created_field) TextView createdByView;
    @BindView(R.id.total_size_field) TextView torrentSizeView;
    @BindView(R.id.date_field) TextView creationDateView;
    @BindView(R.id.count_files_field) TextView fileCountView;
    @BindView(R.id.storage_field) TextView pathToUploadView;
    @BindView(R.id.storage_size) TextView freeSpaceView;
    @BindView(R.id.torrent_added) TextView torrentAddedView;
    @BindView(R.id.layout_torrent_comment) LinearLayout commentViewLayout;
    @BindView(R.id.layout_torrent_created_in_program) LinearLayout createdByViewLayout;
    @BindView(R.id.layout_torrent_size_and_count) LinearLayout sizeAndCountViewLayout;
    @BindView(R.id.layout_torrent_create_date) LinearLayout creationDateViewLayout;
    @BindView(R.id.btn_choose_folder) ImageButton folderChooserButton;
    @BindView(R.id.sequential_checkbox) CheckBox sequentialDownload;
    @BindView(R.id.seed_text) TextView seedText;
    @BindView(R.id.seed_image) ImageView seedImage;

    @BindViews({R.id.seed_text, R.id.seed_image, R.id.btn_seed, R.id.seed_space}) List<View> btnSeed;

    @BindString(R.string.seed_start) String startSeed;
    @BindString(R.string.seed_stop) String stopSeed;
    @BindDrawable(R.drawable.ic_start_seed) Drawable startSeedImage;
    @BindDrawable(R.drawable.ic_stop_seed) Drawable stopSeedImage;

    private Unbinder unbinder;


    public static DetailTorrentInfoFragment newInstance(Torrent torrent, TorrentMetaInfo info, TorrentStateParcel state) {
        DetailTorrentInfoFragment fragment = new DetailTorrentInfoFragment();
        fragment.info = info;
        fragment.torrent = torrent;
        fragment.state = state;

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    /* For API < 23 */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof AppCompatActivity) {
            this.activity = (AppCompatActivity) activity;

            if (activity instanceof DetailTorrentFragment.Callback) {
                callback = (DetailTorrentFragment.Callback) activity;
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        callback = null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            info = savedInstanceState.getParcelable(TAG_INFO);
            torrent = savedInstanceState.getParcelable(TAG_TORRENT);
            downloadDir = savedInstanceState.getString(TAG_DOWNLOAD_DIR);
            name = savedInstanceState.getString(TAG_NAME);
            isSequentialDownload = savedInstanceState.getBoolean(TAG_IS_SEQUENTIAL);

        } else if (torrent != null) {
            downloadDir = torrent.getDownloadPath();
            name = torrent.getName();
            isSequentialDownload = torrent.isSequentialDownload();
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fmt_detail_torrent_info, container, false);
        unbinder = ButterKnife.bind(this, view);
        initFields();
        initTorrentState();
        return view;
    }

    private void initTorrentState() {
        if (state != null) {
            toggleState(TorrentUtils.isFinished(state.stateCode, state.progress), state.stateCode == TorrentStateCode.SEEDING);
        } else if (torrent != null) {
            toggleState(torrent.isFinished(), !torrent.isPaused());
        }
    }

    private void toggleState(boolean finished, boolean seeding) {
        if (finished) {
            ButterknifeUtils.setVisibility(btnSeed, View.VISIBLE);
            if (seeding) {
                seedText.setText(stopSeed);
                seedImage.setImageDrawable(stopSeedImage);
            } else {
                seedText.setText(startSeed);
                seedImage.setImageDrawable(startSeedImage);
            }
        } else {
            ButterknifeUtils.setVisibility(btnSeed, View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void initFields() {
        if (info == null || torrent == null) {
            return;
        }

        sequentialDownload.setChecked(isSequentialDownload);
        sequentialDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSequentialDownload = sequentialDownload.isChecked();

                if (callback != null) {
                    callback.onTorrentInfoChanged();
                }
            }
        });

        torrentNameField.setText(name);
        sha1HashView.setText(info.sha1Hash);
        pathToUploadView.setText(downloadDir);

        if (TextUtils.isEmpty(info.comment)) {
            commentViewLayout.setVisibility(View.GONE);
        } else {
            commentView.setText(info.comment);
            commentViewLayout.setVisibility(View.VISIBLE);
        }

        if (TextUtils.isEmpty(info.createdBy)) {
            createdByViewLayout.setVisibility(View.GONE);
        } else {
            createdByView.setText(info.createdBy);
            createdByViewLayout.setVisibility(View.VISIBLE);
        }

        if (info.torrentSize == 0 || info.fileCount == 0) {
            sizeAndCountViewLayout.setVisibility(View.GONE);
        } else {
            torrentSizeView.setText(Formatter.formatFileSize(activity, info.torrentSize));
            fileCountView.setText(Integer.toString(info.fileCount));
            freeSpaceView.setText(String.format(getString(R.string.free_space),
                    Formatter.formatFileSize(activity.getApplicationContext(), FileIOUtils.getFreeSpace(torrent.getDownloadPath()))));
            sizeAndCountViewLayout.setVisibility(View.VISIBLE);
        }

        if (info.creationDate == 0) {
            creationDateViewLayout.setVisibility(View.GONE);
        } else {
            creationDateView.setText(SimpleDateFormat.getDateTimeInstance()
                    .format(new Date(info.creationDate)));
            creationDateViewLayout.setVisibility(View.VISIBLE);
        }

        torrentAddedView.setText(SimpleDateFormat.getDateTimeInstance()
                .format(new Date(torrent.getDateAdded())));

        torrentNameField.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                wasChangedManually = true;
                return true;
            } else {
                return false;
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(TAG_INFO, info);
        outState.putParcelable(TAG_TORRENT, torrent);
        outState.putString(TAG_DOWNLOAD_DIR, downloadDir);
        outState.putString(TAG_NAME, name);
        outState.putBoolean(TAG_IS_SEQUENTIAL, isSequentialDownload);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (activity == null) {
            activity = (AppCompatActivity) getActivity();
        }

        folderChooserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, FileManagerDialog.class);

                FileManagerConfig config = new FileManagerConfig(downloadDir,
                        null,
                        null,
                        FileManagerConfig.DIR_CHOOSER_MODE);

                i.putExtra(FileManagerDialog.TAG_CONFIG, config);

                startActivityForResult(i, DIR_CHOOSER_REQUEST);
            }
        });

        torrentNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                checkEditTextField(s);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /* Nothing */
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (torrentNameField.isFocused()) {
                    if (callback != null) {
                        callback.onTorrentInfoChanged();
                    }
                }

                checkEditTextField(s);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DIR_CHOOSER_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data.hasExtra(FileManagerDialog.TAG_RETURNED_PATH)) {
                downloadDir = data.getStringExtra(FileManagerDialog.TAG_RETURNED_PATH);
                pathToUploadView.setText(downloadDir);
                freeSpaceView.setText(
                        String.format(
                                getString(R.string.free_space),
                                Formatter.formatFileSize(activity.getApplicationContext(), FileIOUtils.getFreeSpace(downloadDir))));

                if (callback != null) {
                    callback.onTorrentInfoChanged();
                }
            }
        }
    }

    private void checkEditTextField(CharSequence s) {
        if (TextUtils.isEmpty(s)) {
            layoutTorrentName.setErrorEnabled(true);
            layoutTorrentName.setError(getString(R.string.error_field_required));
            layoutTorrentName.requestFocus();
            if (callback != null) {
                callback.onTorrentInfoChangesUndone();
            }
        } else {
            layoutTorrentName.setErrorEnabled(false);
            layoutTorrentName.setError(null);
            name = s.toString();
        }
    }

    public void setInfo(TorrentMetaInfo info) {
        this.info = info;

        initFields();
    }

    public String getDownloadPath() {
        return downloadDir;
    }

    public void setDownloadPath(String path) {
        if (path == null) {
            return;
        }

        downloadDir = path;
        pathToUploadView.setText(path);
        freeSpaceView.setText(
                String.format(
                        getString(R.string.free_space),
                        Formatter.formatFileSize(activity.getApplicationContext(), FileIOUtils.getFreeSpace(path))));
    }

    public String getTorrentName() {
        if (torrentNameField != null && torrentNameField.getText().toString().trim().length() > 0) {
            return torrentNameField.getText().toString().trim();
        } else {
            return name;
        }
    }

    public void setTorrentName(String name) {
        if (name == null) {
            return;
        }

        this.name = name;
        torrentNameField.setText(name);
    }

    public void updateTorrentName(String name) {
        if (wasChangedManually && torrentNameField.hasFocus()) {
            return;
        }
        setTorrentName(name);
    }

    public boolean isSequentialDownload() {
        return isSequentialDownload;
    }

    public void setSequentialDownload(boolean sequential) {
        isSequentialDownload = sequential;
        sequentialDownload.setChecked(sequential);
    }

    public void setState(TorrentStateParcel state) {
        this.state = state;
        if (isAdded()) {
            initTorrentState();
        }
    }

    @OnClick({R.id.btn_seed})
    public void onSeedBtnClicked() {
        if (callback != null) {
            callback.onTorrentPlayPauseClicked();
        }
    }
}