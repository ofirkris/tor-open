package co.we.torrent.presentation.main.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import co.we.torrent.navigation.AppRouter;
import co.we.torrent.presentation.main.view.TorrentsView;

@InjectViewState
public class TorrentsPresenter extends MvpPresenter<TorrentsView> {
    private final AppRouter appRouter;
    private final int type;

    public TorrentsPresenter(AppRouter appRouter, int type) {
        this.appRouter = appRouter;
        this.type = type;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void exit() {
        appRouter.exit();
    }

    public void toggleDeleteMode(boolean value) {
        getViewState().toggleDeleteMode(value);
    }
}
