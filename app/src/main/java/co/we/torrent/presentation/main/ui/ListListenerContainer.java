package co.we.torrent.presentation.main.ui;

import javax.inject.Inject;

import co.we.torrent.data.core.stateparcel.TorrentStateParcel;
import co.we.torrent.di.scopes.Main;

/**
 * Created 09.01.2018 by
 *
 * @author Alexander Artemov
 */
@Main
public class ListListenerContainer implements TorrentsAdapter.ViewHolder.ClickListener {

    private TorrentsAdapter.ViewHolder.ClickListener listener;

    @Inject
    public ListListenerContainer() {
    }

    public void attachListener(TorrentsAdapter.ViewHolder.ClickListener listener){
        this.listener = listener;
    }

    public void detachListener(){
        listener = null;
    }

    @Override
    public void onItemClicked(int position, TorrentStateParcel torrentState) {
        if (listener != null) {
            listener.onItemClicked(position, torrentState);
        }
    }

    @Override
    public void onItemLongClicked(int position, TorrentStateParcel torrentState) {
        if (listener != null) {
            listener.onItemLongClicked(position, torrentState);
        }
    }

    @Override
    public void onPauseButtonClicked(int position, TorrentStateParcel torrentState) {
        if (listener != null) {
            listener.onPauseButtonClicked(position, torrentState);
        }
    }

    @Override
    public void onInfoClicked(int position, TorrentStateParcel torrentState) {
        if (listener != null) {
            listener.onInfoClicked(position, torrentState);
        }

    }
}
