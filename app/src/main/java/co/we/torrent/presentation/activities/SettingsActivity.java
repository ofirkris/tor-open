package co.we.torrent.presentation.activities;

import android.os.Bundle;
import android.widget.TextView;

import co.we.torrent.R;
import co.we.torrent.presentation.settings.BasePreferenceActivity;
import co.we.torrent.presentation.settings.SettingsFragment;

public class SettingsActivity extends BasePreferenceActivity implements SettingsFragment.Callback {

    private static final String TAG_TITLE = "title";

    private TextView detailTitle;
    private String title;
    private boolean tablet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.settings));

        detailTitle = findViewById(R.id.detail_title);
        tablet = findViewById(R.id.detail_fragment_container) != null;

        if (savedInstanceState == null) {
            setFragment(SettingsFragment.newInstance());
        } else {
            title = savedInstanceState.getString(TAG_TITLE);
            if (title != null && detailTitle != null) {
                detailTitle.setText(title);
            }
        }
    }

    public boolean isTablet(){
        return tablet;
    }

    @Override
    public void onDetailTitleChanged(String title) {
        this.title = title;

        if (detailTitle != null && title != null) {
            detailTitle.setText(title);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(TAG_TITLE, title);

        super.onSaveInstanceState(outState);
    }
}
