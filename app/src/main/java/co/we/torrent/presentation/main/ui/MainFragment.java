package co.we.torrent.presentation.main.ui;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.crashlytics.android.Crashlytics;
import com.frostwire.jlibtorrent.Priority;
import com.frostwire.jlibtorrent.TorrentInfo;
import com.frostwire.jlibtorrent.swig.add_torrent_params;
import com.frostwire.jlibtorrent.swig.error_code;
import com.frostwire.jlibtorrent.swig.sha1_hash;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import co.we.torrent.AndroidApplication;
import co.we.torrent.R;
import co.we.torrent.data.core.Torrent;
import co.we.torrent.data.core.TorrentDownload;
import co.we.torrent.data.core.TorrentEngine;
import co.we.torrent.data.core.TorrentMetaInfo;
import co.we.torrent.data.core.exceptions.DecodeException;
import co.we.torrent.data.core.exceptions.FetchLinkException;
import co.we.torrent.data.core.exceptions.FileAlreadyExistsException;
import co.we.torrent.data.core.exceptions.FreeSpaceException;
import co.we.torrent.data.core.exceptions.WifiOnlyException;
import co.we.torrent.data.core.filetree.BencodeFileTree;
import co.we.torrent.data.core.stateparcel.TorrentStateParcel;
import co.we.torrent.data.services.TorrentTaskService;
import co.we.torrent.models.TorrentListType;
import co.we.torrent.navigation.BackButtonListener;
import co.we.torrent.other.BencodeFileTreeUtils;
import co.we.torrent.other.FileIOUtils;
import co.we.torrent.other.FileStorageUtils;
import co.we.torrent.other.KeyboardUtils;
import co.we.torrent.other.ShowCaseInstance;
import co.we.torrent.other.ShowCaseUtils;
import co.we.torrent.other.TorrentUtils;
import co.we.torrent.other.Utils;
import co.we.torrent.presentation.activities.AddTorrentState;
import co.we.torrent.presentation.activities.DetailTorrentActivity;
import co.we.torrent.presentation.base.BaseFragment;
import co.we.torrent.presentation.custom_views.LockViewPager;
import co.we.torrent.presentation.main.presenter.MainPresenter;
import co.we.torrent.presentation.main.view.MainView;
import co.we.torrent.presentation.old.dialogs.BaseAlertDialog;
import co.we.torrent.presentation.old.dialogs.filemanager.FileManagerConfig;
import co.we.torrent.presentation.old.dialogs.filemanager.FileManagerDialog;
import timber.log.Timber;

import static android.app.Activity.RESULT_OK;
import static co.we.torrent.presentation.activities.AddTorrentState.DECODE_TORRENT_COMPLETED;
import static co.we.torrent.presentation.activities.AddTorrentState.DECODE_TORRENT_FILE;
import static co.we.torrent.presentation.activities.AddTorrentState.ERROR;
import static co.we.torrent.presentation.activities.AddTorrentState.FETCHING_HTTP;
import static co.we.torrent.presentation.activities.AddTorrentState.FETCHING_HTTP_COMPLETED;
import static co.we.torrent.presentation.activities.AddTorrentState.FETCHING_MAGNET;
import static co.we.torrent.presentation.activities.AddTorrentState.UNKNOWN;

public class MainFragment extends BaseFragment implements MainView, BackButtonListener,
        ViewPager.OnPageChangeListener, BaseAlertDialog.OnDialogShowListener, BaseAlertDialog.OnClickListener, TorrentsAdapter.ViewHolder.ClickListener {

    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    private static final int TORRENT_FILE_CHOOSE_REQUEST = 2;
    private static final int OPEN_FILE_CHOOSE_REQUEST = 4;
    private static final int CHANGE_DOWNLOAD_FOLDER_REQUEST = 5;
    private static final int SHOW_TUTORIAL_REQUEST = 3;
    private static final String TAG_WIFI_DIALOG = "tag_wifi_dialog";
    private static final String TAG_PREV_IMPL_INTENT = "prev_impl_intent";
    private static final String TAG_ADD_LINK_DIALOG = "add_link_dialog";
    private static final String TAG_DELETE_TORRENT_DIALOG = "delete_torrent_dialog";
    private static final String TAG_TURN_OFF_WIFI_ONLY = "tag_off_wifi_only";


    private static final String TAG_DECODE_EXCEPT_DIALOG = "decode_except_dialog";
    private static final String TAG_FETCH_EXCEPT_DIALOG = "fetch_except_dialog";
    private static final String TAG_ILLEGAL_ARGUMENT = "illegal_argument";
    private static final String TAG_SPACE_EXCEPTION = "free space_except_dialog";

    @BindView(R.id.main_coordinator_layout) CoordinatorLayout coordinatorLayout;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.tabs) TabLayout tabs;
    @BindView(R.id.pager) LockViewPager viewPager;
    @BindView(R.id.btn_search) ImageView btnSearch;
    @BindView(R.id.btn_magnet) ImageView btnMagnet;
    @BindView(R.id.btn_delete) ImageView btnDelete;
    @BindView(R.id.btn_menu) ImageView btnMenu;
    @BindView(R.id.add_torrent_button) FloatingActionButton fabAddFiles;
    @BindView(R.id.btn_select_all) ImageView selectAll;
    @BindView(R.id.btn_delete_text) TextView deleteText;
    @BindView(R.id.search_field) AppCompatEditText searchField;
    @BindView(R.id.btn_back) ImageView btnBack;
    @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;

    @InjectPresenter MainPresenter presenter;
    @Inject ListConfig listConfig;
    @Inject ListListenerContainer container;

    private Unbinder unbinder;
    private MainPagerAdapter adapter;

    private AppCompatActivity activity;
    private TorrentsFragment[] fragments;
    private TorrentsFragment currentFragment;
    private boolean openedFirst = true;
    private boolean showDialogAfterTutorial = false;
    // Prevents re-adding the torrent, obtained through implicit intent
    private Intent prevImplIntent;
    private TorrentTaskService service;
    private boolean bound; //Flag indicating whether we have called bind on the service.
    /*
     * Torrents are added to the queue, if the client is not bounded to service.
     * Trying to add torrents will be made at the first connect.
     */
    private HashSet<Torrent> addTorrentsQueue = new HashSet<>();

    /**
     * --------------------------------- Add torrent ---------------------------------------------//
     */
    public static final String UNKNOWN_PATH = "Unknown path to the torrent file";

    private Uri uri;
    private TorrentMetaInfo info;

    private boolean fromMagnet = false;
    private boolean inProcess = false;

    private String pathToTempTorrent = null;
    private AtomicReference<AddTorrentState> decodeState = new AtomicReference<>(UNKNOWN);
    private String downloadDir;
    private String torrentName;

    private BencodeFileTree fileTree;

    /**
     * --------------------------------- Initialization ------------------------------------------//
     */

    @ProvidePresenter
    public MainPresenter providePresenter() {
        return AndroidApplication.mainComponent().getMainPresenter();
    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fmt_main, container, false);
        unbinder = ButterKnife.bind(this, view);
        AndroidApplication.mainComponent().inject(this);
        viewPager.setListConfig(listConfig);
        initializePages(savedInstanceState);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (activity == null) {
            activity = (AppCompatActivity) getActivity();
        }

        activity.bindService(new Intent(activity.getApplicationContext(), TorrentTaskService.class),
                connection, Context.BIND_AUTO_CREATE);

        if (savedInstanceState != null) {
            prevImplIntent = savedInstanceState.getParcelable(TAG_PREV_IMPL_INTENT);
        }

        //If add torrent dialog has been called by an implicit intent
        Intent intent = activity.getIntent();
        if (!inProcess && (prevImplIntent == null || !prevImplIntent.equals(intent))) {
            prevImplIntent = intent;
            if (intent.getData() != null) {
                clearAddTorrentFlow();
                decodeTorrent(intent.getData());
            }
        }
    }

    private void decodeTorrent(Uri uri) {
        if (uri != null) {
            this.setUri(uri);
        }
        Activity activity = getActivity();
        if (activity == null) {
            clearAddTorrentFlow();
            return;
        }

        this.downloadDir = TorrentUtils.getTorrentDownloadPath(activity.getApplicationContext());
        startProcess();
    }

    private void startProcess() {
        inProcess = true;
        if (uri == null || uri.getScheme() == null) {
            handlingException(new IllegalArgumentException("Can't decode link/path"));
            return;
        }

        initDecode();
    }

    private void initDecode() {
        switch (uri.getScheme()) {
            case Utils.FILE_PREFIX:
            case Utils.CONTENT_PREFIX:
                decodeState.set(DECODE_TORRENT_FILE);
                break;
            case Utils.HTTP_PREFIX:
            case Utils.HTTPS_PREFIX:
                decodeState.set(FETCHING_HTTP);
                break;
            case Utils.MAGNET_PREFIX:
                fromMagnet = true;
                decodeState.set(FETCHING_MAGNET);
                break;
            default:
                handlingException(new IllegalArgumentException("Unknown link/path type: " + uri.getScheme()));
                return;
        }

        new TorrentDecodeTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, uri);
    }

    private void updateInfo() {
        if (info == null) {
            return;
        }

        this.torrentName = info.torrentName;
    }

    public void handlingException(Exception e) {
        if (e == null) {
            return;
        }

        if (!(e instanceof FreeSpaceException)) {
            decodeState.set(ERROR);
            clearAddTorrentFlow();
        }

        handleError(e);
    }


    private String getSha(String uri) {
        if (uri == null)
            throw new IllegalArgumentException("Magnet link is null");

        error_code ec = new error_code();
        add_torrent_params p = add_torrent_params.parse_magnet_uri(uri, ec);

        if (ec.value() != 0)
            throw new IllegalArgumentException(ec.message());

        p.set_disabled_storage();
        sha1_hash hash = p.getInfo_hash();
        return hash.to_hex();
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    private class TorrentDecodeTask extends AsyncTask<Uri, Void, Exception> {

        private TorrentDecodeTask() {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Exception doInBackground(Uri... params) {
            Uri uri = params[0];
            try {
                Activity activity = MainFragment.this.getActivity();
                Context context = activity.getApplicationContext();
                switch (uri.getScheme()) {
                    case Utils.FILE_PREFIX:
                        pathToTempTorrent = uri.getPath();
                        break;
                    case Utils.CONTENT_PREFIX:
                        File contentTmp = FileIOUtils.makeTempFile(context, ".torrent");
                        FileIOUtils.copyContentURIToFile(context, uri, contentTmp);

                        if (contentTmp.exists()) {
                            pathToTempTorrent = contentTmp.getAbsolutePath();
                        } else {
                            return new IllegalArgumentException(UNKNOWN_PATH);
                        }
                        break;
                    case Utils.MAGNET_PREFIX:
                        String hash = getSha(uri.toString());

                        if (hash != null) {
                            info = new TorrentMetaInfo(hash, hash);
                        }

                        break;
                    case Utils.HTTP_PREFIX:
                    case Utils.HTTPS_PREFIX:
                        File httpTmp = FileIOUtils.makeTempFile(context, ".torrent");
                        TorrentUtils.fetchByHTTP(context, uri.toString(), httpTmp);

                        if (httpTmp.exists()) {
                            pathToTempTorrent = httpTmp.getAbsolutePath();
                        } else {
                            return new IllegalArgumentException(UNKNOWN_PATH);
                        }

                        break;
                }

                if (pathToTempTorrent != null) {
                    info = new TorrentMetaInfo(pathToTempTorrent);
                }

            } catch (Exception e) {
                return e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Exception e) {

            handlingException(e);

            switch (decodeState.get()) {
                case DECODE_TORRENT_FILE:
                    decodeState.set(DECODE_TORRENT_COMPLETED);
                    break;
                case FETCHING_HTTP:
                    decodeState.set(FETCHING_HTTP_COMPLETED);
                    break;
            }

            if (e != null) {
                return;
            }

            updateInfo();
            if (decodeState.get() != FETCHING_MAGNET) {
                initializeFiles();
            }
            buildTorrent();
        }
    }

    private void initializeFiles() {
        if (info == null || info.fileList == null) {
            return;
        }

        fileTree = BencodeFileTreeUtils.buildFileTree(info.fileList);
        fileTree.select(true);
    }


    private void buildTorrent() {
        if (TextUtils.isEmpty(torrentName) || (info.fileList.size() == 0 && decodeState.get() != FETCHING_MAGNET)) {
            clearAddTorrentFlow();
            return;
        }

        ArrayList<Priority> priorities = new ArrayList<>(Collections.nCopies(info.fileList.size(), Priority.NORMAL));

        if ((priorities.size() == 0) && decodeState.get() != FETCHING_MAGNET) {
            handlingException(new IOException());
            return;
        }

        if (fileTree != null) {
            long freeSpace = FileIOUtils.getFreeSpace(downloadDir);
            long torrentSize = fileTree.selectedFileSize();
            if (freeSpace < torrentSize) {
                handlingException(new FreeSpaceException("Not enough free space in " + downloadDir + " : "
                        + freeSpace + " free, but torrent size is " + torrentSize, torrentSize, freeSpace));
                return;
            }
        }

        Torrent torrent = new Torrent(info.sha1Hash, torrentName, priorities,
                downloadDir, System.currentTimeMillis());

        torrent.setSequentialDownload(false);
        torrent.setPaused(false);
        torrent.setTorrentFilePath(pathToTempTorrent == null && fromMagnet ? uri.toString() : pathToTempTorrent);
        torrent.setDownloadingMetadata(fromMagnet);

        pushTorrent(torrent);
        showMessage(R.string.torrent_created);
        clearAddTorrentFlow();
    }

    private void showMessage(int id) {
        Activity activity = getActivity();
        if (activity != null) {
            Toast.makeText(activity, getString(id), Toast.LENGTH_SHORT).show();
        }
    }

    private void clearAddTorrentFlow() {
        uri = null;
        info = null;
        fromMagnet = false;
        pathToTempTorrent = null;
        decodeState = new AtomicReference<>(UNKNOWN);
        downloadDir = null;
        torrentName = null;
        fileTree = null;
        inProcess = false;
    }

    /**
     * -------------------------------------------------------------------------------------------//
     */

    private void pushTorrent(Torrent torrent) {
        if (torrent != null) {
            if (!bound || service == null) {
                addTorrentsQueue.add(torrent);
            } else {
                try {
                    service.addTorrent(torrent, false);
                } catch (Throwable e) {
                    Timber.e(e);
                    addTorrentError(e);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        container.attachListener(this);
        clearLoadingDialog();
        if (openedFirst) {
            openedFirst = false;
            waitVisibleFor(presenter::afterFirstOpenedWithDelay);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(TAG_PREV_IMPL_INTENT, prevImplIntent);
        super.onSaveInstanceState(outState);
    }

    public void initializePages(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            if (adapter == null || fragments == null || fragments.length != 3) {
                fragments = new TorrentsFragment[3];
                fragments[TorrentListType.ALL] = TorrentsFragment.newInstance(TorrentListType.ALL);
                fragments[TorrentListType.QUEUED] = TorrentsFragment.newInstance(TorrentListType.QUEUED);
                fragments[TorrentListType.FINISHED] = TorrentsFragment.newInstance(TorrentListType.FINISHED);
            }
        } else {
            fragments = new TorrentsFragment[3];
            List<Fragment> list = getChildFragmentManager().getFragments();
            for (Fragment fragment : list) {
                Bundle args = fragment.getArguments();
                if (args != null && args.containsKey(TorrentsFragment.TYPE)) {
                    int type = args.getInt(TorrentsFragment.TYPE);
                    fragments[type] = (TorrentsFragment) fragment;
                }
            }
            addFragmentIfNull(TorrentListType.ALL);
            addFragmentIfNull(TorrentListType.QUEUED);
            addFragmentIfNull(TorrentListType.FINISHED);
        }

        String[] titles = {getString(R.string.all), getString(R.string.queued), getString(R.string.finished)};


        adapter = new MainPagerAdapter(getChildFragmentManager(), fragments, titles);


        viewPager.addOnPageChangeListener(this);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(fragments.length);

        tabs.setupWithViewPager(viewPager);

        currentFragment = fragments[viewPager.getCurrentItem()];
    }

    private void addFragmentIfNull(int type) {
        if (fragments[type] == null) {
            fragments[type] = TorrentsFragment.newInstance(type);
        }
    }

    private void toggleMenuBtn(boolean visible) {
        if (visible) {
            btnMenu.setVisibility(View.VISIBLE);
            btnBack.setVisibility(View.GONE);
        } else {
            btnMenu.setVisibility(View.GONE);
            btnBack.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        closeDialog(TAG_WIFI_DIALOG);
        closeDialog(TAG_ADD_LINK_DIALOG);
        closeDialog(TAG_DELETE_TORRENT_DIALOG);

        if (bound) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                getActivity().unbindService(connection);
            }
            bound = false;
        }

        unbinder.unbind();
    }

    private void closeDialog(String tag) {
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager != null ? fragmentManager.findFragmentByTag(tag) : null;
        if (fragment != null) {
            DialogFragment df = (DialogFragment) fragment;
            df.dismissAllowingStateLoss();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        container.detachListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        clearLoadingDialog();
    }

    private TorrentsAdapter getAdapter() {
        return currentFragment.getAdapter();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //Do nothing
    }

    @Override
    public void onPageSelected(int position) {
        this.currentFragment = fragments[position];
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //Do nothing
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.i("RESULT MAIN/ %s %s %s", requestCode, requestCode, data);
        switch (requestCode) {
            case TORRENT_FILE_CHOOSE_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data.hasExtra(FileManagerDialog.TAG_RETURNED_PATH)) {
                        String path = data.getStringExtra(FileManagerDialog.TAG_RETURNED_PATH);

                        try {
                            decodeTorrent(Uri.fromFile(new File(path)));
                        } catch (Exception e) {
                            handleError(e, R.string.error_open_torrent_file);
                        }
                    }
                }
                break;
            case SHOW_TUTORIAL_REQUEST:
                if (showDialogAfterTutorial) {
                    this.showDialogAfterTutorial = false;
                    showWifiTorrentDialog();
                }
                break;
            case OPEN_FILE_CHOOSE_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data.hasExtra(FileManagerDialog.TAG_RETURNED_PATH)) {
                        String path = data.getStringExtra(FileManagerDialog.TAG_RETURNED_PATH);
                        File file = new File(path);
                        if (file.isFile()) {
                            FileStorageUtils.openFile(file, activity, true, true);
                        }
                    }
                }

                break;

            case CHANGE_DOWNLOAD_FOLDER_REQUEST:
                if (resultCode == RESULT_OK) {
                    if (data.hasExtra(FileManagerDialog.TAG_RETURNED_PATH)) {
                        downloadDir = data.getStringExtra(FileManagerDialog.TAG_RETURNED_PATH);
                        buildTorrent();
                    }
                } else {
                    clearAddTorrentFlow();
                }

                break;
        }
    }

    private void showWifiTorrentDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            BaseAlertDialog wifiDialog = BaseAlertDialog.newInstance(
                    getString(R.string.wifi_title),
                    getString(R.string.wifi_message),
                    R.layout.dialog_wifi_torrent,
                    getString(R.string.yes),
                    getString(R.string.no),
                    null,
                    MainFragment.this);

            wifiDialog.show(fragmentManager, TAG_WIFI_DIALOG);
        }
    }

    private void handleError(Exception e) {
        Crashlytics.logException(e);
        Timber.e(e);
        FragmentManager fm = getFragmentManager();
        if (fm == null)
            return;
        if (e instanceof DecodeException) {
            BaseAlertDialog errDialog = BaseAlertDialog.newInstance(
                    getString(R.string.error),
                    getString(R.string.error_decode_torrent),
                    0,
                    getString(R.string.ok),
                    null,
                    null,
                    this);

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(errDialog, TAG_DECODE_EXCEPT_DIALOG);
            ft.commitAllowingStateLoss();
        } else if (e instanceof FetchLinkException) {
            BaseAlertDialog errDialog = BaseAlertDialog.newInstance(
                    getString(R.string.error),
                    getString(R.string.error_fetch_link),
                    0,
                    getString(R.string.ok),
                    null,
                    null,
                    this);

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(errDialog, TAG_FETCH_EXCEPT_DIALOG);
            ft.commitAllowingStateLoss();
        } else if (e instanceof IllegalArgumentException) {
            BaseAlertDialog errDialog = BaseAlertDialog.newInstance(
                    getString(R.string.error),
                    getString(R.string.error_invalid_link_or_path),
                    0,
                    getString(R.string.ok),
                    null,
                    null,
                    this);

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(errDialog, TAG_ILLEGAL_ARGUMENT);
            ft.commitAllowingStateLoss();
        } else if (e instanceof FreeSpaceException) {
            String message = getString(R.string.error_free_space);
            if (((FreeSpaceException) e).hasInfo) {
                Context context = getContext();
                long torrentSize = ((FreeSpaceException) e).torrentSize;
                long totalSize = ((FreeSpaceException) e).totalSize;
                message += "\nFree space: " + Formatter.formatShortFileSize(context, totalSize) + "\nTorrent size: " + Formatter.formatShortFileSize(context, torrentSize);
            }
            BaseAlertDialog errDialog = BaseAlertDialog.newInstance(
                    getString(R.string.error),
                    message,
                    0,
                    getString(R.string.ok),
                    getString(R.string.change_path),
                    null,
                    this);

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(errDialog, TAG_SPACE_EXCEPTION);
            ft.commitAllowingStateLoss();
        } else if (e instanceof WifiOnlyException) {
            BaseAlertDialog errDialog = BaseAlertDialog.newInstance(
                    getString(R.string.error),
                    getString(R.string.resume_without_wifi),
                    0,
                    getString(R.string.yes),
                    getString(R.string.no),
                    null,
                    this);

            FragmentTransaction ft = fm.beginTransaction();
            ft.add(errDialog, TAG_TURN_OFF_WIFI_ONLY);
            ft.commitAllowingStateLoss();
        } else {
            showMessage(R.string.error_io_torrent);
        }
    }

    private void addTorrentError(Throwable e) {
        if (e == null || !isAdded())
            return;

        if (e instanceof FileNotFoundException) {
            handleError(e, R.string.error_file_not_found_add_torrent);
        } else if (e instanceof IOException) {
            handleError(e, R.string.error_io_add_torrent);
        } else if (e instanceof FileAlreadyExistsException) {
            Snackbar.make(coordinatorLayout,
                    R.string.torrent_exist,
                    Snackbar.LENGTH_LONG)
                    .show();
        } else {
            handleError(e, R.string.error_add_torrent);
        }
    }

    private void handleError(Throwable e, int messageId) {
        showMessage(messageId);
        Crashlytics.logException(e);
        Timber.e(e);
    }

    /**
     * ------------------------------------ Dialogs ----------------------------------------------//
     */

    @Override
    public void onShow(final AlertDialog dialog) {
        FragmentManager fragmentManager = getFragmentManager();
        if (dialog != null && fragmentManager != null) {
            if (fragmentManager.findFragmentByTag(TAG_ADD_LINK_DIALOG) != null) {
                initAddDialog(dialog);
            }
        }
    }

    private void initAddDialog(final AlertDialog dialog) {
        final TextInputEditText field = dialog.findViewById(R.id.text_input_dialog);
        final TextInputLayout fieldLayout = dialog.findViewById(R.id.layout_text_input_dialog);

        /* Dismiss error label if user has changed the text */
        if (field != null && fieldLayout != null) {
            field.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    /* Nothing */
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    fieldLayout.setErrorEnabled(false);
                    fieldLayout.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    /* Nothing */
                }
            });
        }

        /*
         * It is necessary in order to the dialog is not closed by
         * pressing positive button if the text checker gave a false result
         */
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);

        positiveButton.setOnClickListener(v -> {
            if (field != null && fieldLayout != null) {
                String link = field.getText().toString();

                if (checkEditTextField(link, fieldLayout)) {
                    String url;

                    if (link.startsWith(Utils.MAGNET_PREFIX)) {
                        url = link;
                    } else if (link.startsWith(Utils.HTTP_PREFIX)
                            || link.startsWith(Utils.HTTPS_PREFIX)) {
                        url = Utils.normalizeURL(link);
                    } else {
                        url = Utils.INFOHASH_PREFIX + link;
                    }

                    if (url != null) {
                        decodeTorrent(Uri.parse(url));
                    }

                    dialog.dismiss();
                }
            }
        });

        /* Inserting a link from the clipboard */
        String clipboard = Utils.getClipboard(activity.getApplicationContext());
        String url;

        if (clipboard != null) {
            if (!clipboard.startsWith(Utils.MAGNET_PREFIX)) {
                url = Utils.normalizeURL(clipboard);
            } else {
                url = clipboard;
            }

            if (field != null && url != null) {
                field.setText(url);
            }
        }
    }

    private boolean checkEditTextField(String s, TextInputLayout layout) {
        if (s == null || layout == null) {
            return false;
        }

        if (TextUtils.isEmpty(s)) {
            layout.setErrorEnabled(true);
            layout.setError(getString(R.string.error_empty_link));
            layout.requestFocus();

            return false;
        }

        if (s.startsWith(Utils.MAGNET_PREFIX)) {
            layout.setErrorEnabled(false);
            layout.setError(null);

            return true;
        }

        if (s.startsWith(Utils.HTTP_PREFIX) || s.startsWith(Utils.HTTPS_PREFIX)) {
            if (!Utils.isValidUrl(s)) {
                layout.setErrorEnabled(true);
                layout.setError(getString(R.string.error_invalid_link));
                layout.requestFocus();

                return false;
            }
        }

        layout.setErrorEnabled(false);
        layout.setError(null);

        return true;
    }

    @OnClick(R.id.btn_magnet)
    public void showAddLinkDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null && fragmentManager.findFragmentByTag(TAG_ADD_LINK_DIALOG) == null) {
            BaseAlertDialog addLinkDialog = BaseAlertDialog.newInstance(
                    getString(R.string.dialog_add_link_title),
                    null,
                    R.layout.dialog_text_input,
                    getString(R.string.ok),
                    getString(R.string.cancel),
                    null,
                    this);

            fragmentManager
                    .beginTransaction()
                    .add(addLinkDialog, TAG_ADD_LINK_DIALOG)
                    .commitAllowingStateLoss();
        }
    }

    @OnClick(R.id.add_torrent_button)
    void onAddTorentClicked() {
        this.showTorrentFileChooserDialog();
    }

    private void showTorrentFileChooserDialog() {
        Intent i = new Intent(activity, FileManagerDialog.class);

        List<String> fileType = new ArrayList<>();
        fileType.add("torrent");
        FileManagerConfig config = new FileManagerConfig(null,
                getString(R.string.torrent_file_chooser_title),
                fileType,
                FileManagerConfig.FILE_CHOOSER_MODE);

        i.putExtra(FileManagerDialog.TAG_CONFIG, config);

        startActivityForResult(i, TORRENT_FILE_CHOOSE_REQUEST);
    }

    private void showDetailTorrent(String id) {
        Intent i = new Intent(activity, DetailTorrentActivity.class);
        i.putExtra(DetailTorrentActivity.TAG_TORRENT_ID, id);
        startActivity(i);
    }

    private void showDeleteDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager != null && fragmentManager.findFragmentByTag(TAG_DELETE_TORRENT_DIALOG) == null) {
            ArrayList<Integer> indexes = getAdapter().getSelectedItems();
            BaseAlertDialog deleteTorrentDialog = BaseAlertDialog.newInstance(
                    getString(R.string.deleting),
                    (indexes.size() > 1 ? getString(R.string.delete_selected_torrents) : getString(R.string.delete_selected_torrent)),
                    R.layout.dialog_delete_torrent,
                    getString(R.string.ok),
                    getString(R.string.cancel),
                    null,
                    MainFragment.this);

            try {
                deleteTorrentDialog.show(fragmentManager, TAG_DELETE_TORRENT_DIALOG);
            } catch (IllegalStateException ignore) {
                Crashlytics.logException(ignore);
                Timber.e(ignore);
            }
        }
    }

    /**
     * --------------------------------- Torrent service------------------------------------------//
     */
    private ServiceConnection connection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            TorrentTaskService.LocalBinder binder = (TorrentTaskService.LocalBinder) service;
            MainFragment.this.service = binder.getService();
            bound = true;

            if (!addTorrentsQueue.isEmpty()) {
                for (Torrent torrent : addTorrentsQueue) {
                    try {
                        MainFragment.this.service.addTorrent(torrent, false);
                    } catch (Throwable e) {
                        Timber.e(e);
                        addTorrentError(e);
                    }
                }
                addTorrentsQueue.clear();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            bound = false;
        }
    };

    /**
     * ------------------------------------- Actions ---------------------------------------------//
     */
    @OnClick(R.id.btn_menu)
    public void onMenuClicked() {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @OnClick(R.id.btn_settings)
    public void onSettingsClicked() {
        drawerLayout.closeDrawer(GravityCompat.START);
        presenter.onSettingsClicked();
    }

    @OnClick(R.id.btn_share)
    public void onShareClicked() {
        drawerLayout.closeDrawer(GravityCompat.START);
        presenter.onShareClicked();
    }

    @OnClick(R.id.btn_help)
    public void onHelpClicked() {
        drawerLayout.closeDrawer(GravityCompat.START);
        presenter.onHelpClicked();
    }

    @OnClick(R.id.btn_privacy)
    public void onPrivacyPolicy() {
        drawerLayout.closeDrawer(GravityCompat.START);
        presenter.onPrivacyClicked();
    }


    @OnClick({R.id.btn_delete_text, R.id.btn_delete})
    public void onDeleteTextClicked() {
        if (listConfig.isDeleteMode()) {
            showDeleteDialog();
        } else {
            setDeleteMode();
        }
    }

    @OnClick(R.id.btn_search)
    public void onSearchClicked() {
        if (!listConfig.isSearchMode()) {
            presenter.toggleSearchMode(true);
        }
    }

    @OnClick(R.id.btn_select_all)
    public void onSelectAllClicked() {
        getAdapter().selectAll();
        changeSelectedTitle();
    }

    @OnClick(R.id.btn_back)
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (listConfig.isDeleteMode()) {
            presenter.toggleDeleteMode(false);
        } else if (listConfig.isSearchMode()) {
            presenter.toggleSearchMode(false);
        } else {
            presenter.exit();
        }
    }

    /**
     * ------------------------------- Delete and search modes------------------------------------//
     */
    private void setDeleteMode() {
        presenter.toggleDeleteMode(true);
        Snackbar mySnackbar = Snackbar.make(coordinatorLayout, R.string.snackbar_delete_mode_opened, Snackbar.LENGTH_LONG);
        mySnackbar.show();
    }

    @Override
    public void toggleDeleteMode(boolean isDeleteMode) {
        listConfig.setDeleteMode(isDeleteMode);
        if (isDeleteMode) {
            toggleBottomTabs(false);
            btnDelete.setVisibility(View.INVISIBLE);
            selectAll.setVisibility(View.VISIBLE);
            deleteText.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            searchField.setVisibility(View.INVISIBLE);
            toggleMenuBtn(false);
            btnSearch.setVisibility(View.INVISIBLE);
            btnMagnet.setVisibility(View.INVISIBLE);
            changeSelectedTitle();
        } else {
            toggleBottomTabs(true);
            selectAll.setVisibility(View.INVISIBLE);
            deleteText.setVisibility(View.INVISIBLE);
            toggleMenuBtn(true);
            toggleSearchMode(listConfig.isSearchMode());
            title.setText(R.string.app_name);
        }
    }

    @Override
    public void toggleSearchMode(boolean isSearchMode) {
        listConfig.setSearchMode(isSearchMode);
        if (isSearchMode) {
            toggleBottomTabs(false);
            listenSearchable();
            btnSearch.setVisibility(View.INVISIBLE);
            btnDelete.setVisibility(View.INVISIBLE);
            btnMagnet.setVisibility(View.INVISIBLE);
            title.setVisibility(View.INVISIBLE);
            toggleMenuBtn(false);
            searchField.setVisibility(View.VISIBLE);
            KeyboardUtils.showKeyboard(getActivity(), searchField);
        } else {
            toggleBottomTabs(true);
            KeyboardUtils.hideKeyboard(getActivity());
            btnSearch.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);
            btnMagnet.setVisibility(View.VISIBLE);
            title.setVisibility(View.VISIBLE);
            toggleMenuBtn(true);
            searchField.setVisibility(View.INVISIBLE);
        }
    }

    private interface OnVisibleListener {
        void onVisible();
    }

    private void waitVisibleFor(final OnVisibleListener listener) {
        final Handler waiter = new Handler();
        waiter.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isVisible()) {
                    waiter.postDelayed(listener::onVisible, 100);
                } else {
                    waiter.postDelayed(this, 30);
                }
            }
        }, 30);
    }

    @Override
    public void showTutorial(boolean showDialogAfterTutorial, int delay) {
        this.showDialogAfterTutorial = showDialogAfterTutorial;
        Resources resources = getResources();
        int showcasePadding = (int) (resources.getDimension(R.dimen.showcase_padding));

        ShowCaseInstance[] showcases = {
                new ShowCaseInstance(btnMagnet,
                        getString(R.string.magnet_links_title_showcase),
                        getString(R.string.magnet_links_showcase),
                        showcasePadding,
                        true
                ),
                new ShowCaseInstance(fabAddFiles,
                        getString(R.string.pick_files_title_showcase),
                        getString(R.string.pick_files_showcase),
                        showcasePadding,
                        false
                ),
                new ShowCaseInstance(btnSearch,
                        getString(R.string.search_title_showcase),
                        getString(R.string.search_showcase),
                        showcasePadding,
                        true
                ),
        };

        showcases[1].setSetGotItAboveView(true);

        ShowCaseUtils.showShowCaseSequence(
                getActivity(),
                this,
                SHOW_TUTORIAL_REQUEST,
                delay,
                showcases
        );

    }

    private void listenSearchable() {
        searchField.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                presenter.goToSearch(v.getText().toString());
                return true;
            }
            return false;
        });
    }

    private void toggleBottomTabs(boolean enable) {
        LinearLayout tabStrip = ((LinearLayout) tabs.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setOnTouchListener((v, event) -> !enable);
        }
    }

    private void changeSelectedTitle() {
        String selected;
        try {
            selected = getString(R.string.selected) + " " + getAdapter().getSelectedCount();
        } catch (Exception e) {
            selected = getString(R.string.selected) + " ";
        }
        title.setText(selected);
    }

    @Override
    public void onItemClicked(int position, TorrentStateParcel torrentState) {
        if (listConfig.isDeleteMode()) {
            getAdapter().toggleSelect(torrentState.torrentId, position);
            if (getAdapter().getSelectedCount() > 0) {
                changeSelectedTitle();
            } else {
                presenter.toggleDeleteMode(false);
            }
        } else if (TorrentUtils.isFinished(torrentState.stateCode, torrentState.progress)) {
            tryToOpenTorrent(torrentState);
        } else {
            showDetailTorrent(torrentState.torrentId);
        }
    }

    public void tryToOpenTorrent(TorrentStateParcel torrentState) {
        FragmentActivity activity = getActivity();
        TorrentDownload task = TorrentEngine.getInstance().getTask(torrentState.torrentId);
        if (task == null || activity == null) return;

        TorrentInfo ti = task.getTorrentInfo();
        TorrentMetaInfo info = null;
        try {
            if (ti != null) {
                info = new TorrentMetaInfo(ti);
            } else {
                info = new TorrentMetaInfo(task.getTorrent().getName(), task.getInfoHash());

            }
        } catch (DecodeException e) {
            String error = getString(R.string.decode_error);
            Timber.e(e, error);
            Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
        }

        if (info != null) {
            String path = task.getTorrent().getDownloadPath() + "/" + info.torrentName;
            File file = new File(path);
            if (!file.isDirectory()) {
                FileStorageUtils.openFile(file, activity, true, true);
            } else {
                Intent i = new Intent(activity, FileManagerDialog.class);

                List<String> fileType = new ArrayList<>();
                FileManagerConfig config = new FileManagerConfig(file.getPath(),
                        activity.getString(R.string.file_chooser_title),
                        fileType,
                        FileManagerConfig.FILE_CHOOSER_MODE);

                i.putExtra(FileManagerDialog.TAG_CONFIG, config);
                startActivityForResult(i, OPEN_FILE_CHOOSE_REQUEST);
            }
        }
    }

    @Override
    public void onItemLongClicked(int position, TorrentStateParcel torrentState) {
        if (!listConfig.isDeleteMode()) {
            setDeleteMode();
            onItemClicked(position, torrentState);
        }
    }

    @Override
    public void onPauseButtonClicked(int position, TorrentStateParcel torrentState) {
        if (!bound || service == null) {
            return;
        }

        try {
            service.pauseResumeTorrent(torrentState.torrentId);
        } catch (WifiOnlyException e) {
            handleError(e);
        }
    }

    @Override
    public void onInfoClicked(int position, TorrentStateParcel torrentState) {
        showDetailTorrent(torrentState.torrentId);
    }

    @Override
    public void onPositiveClicked(@Nullable View v) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;

        if (fm.findFragmentByTag(TAG_DELETE_TORRENT_DIALOG) != null) {

            boolean withFiles = v != null && ((CheckBox) v.findViewById(R.id.dialog_delete_torrent_with_downloaded_files)).isChecked();
            if (bound && service != null) {
                service.deleteTorrents(getAdapter().getSelectedList(), withFiles);
            }

            presenter.toggleDeleteMode(false);
        } else if (fm.findFragmentByTag(TAG_WIFI_DIALOG) != null) {
            presenter.updateWifiOnly(true);
        } else if (fm.findFragmentByTag(TAG_TURN_OFF_WIFI_ONLY) != null) {
            presenter.updateWifiOnly(false);
        } else if (fm.findFragmentByTag(TAG_SPACE_EXCEPTION) != null) {
            clearAddTorrentFlow();
        }
    }

    @Override
    public void onNegativeClicked(@Nullable View v) {
        FragmentManager fm = getFragmentManager();
        if (fm == null) return;

        if (fm.findFragmentByTag(TAG_WIFI_DIALOG) != null) {
            presenter.updateWifiOnly(false);
        } else if (fm.findFragmentByTag(TAG_SPACE_EXCEPTION) != null) {
            Intent i = new Intent(activity, FileManagerDialog.class);

            List<String> fileType = new ArrayList<>();
            FileManagerConfig config = new FileManagerConfig(TorrentUtils.getTorrentDownloadPath(activity.getApplicationContext()),
                    activity.getString(R.string.dir_chooser_title),
                    fileType,
                    FileManagerConfig.DIR_CHOOSER_MODE);

            i.putExtra(FileManagerDialog.TAG_CONFIG, config);
            startActivityForResult(i, CHANGE_DOWNLOAD_FOLDER_REQUEST);
        }
    }

    @Override
    public void onNeutralClicked(@Nullable View v) {

    }
}
