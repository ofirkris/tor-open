package co.we.torrent.presentation.activities;

/**
 * Created 08/04/2018 by
 *
 * @author Alexander Artemov
 */
public enum AddTorrentState {
    UNKNOWN,
    DECODE_TORRENT_FILE,
    DECODE_TORRENT_COMPLETED,
    FETCHING_MAGNET,
    FETCHING_HTTP,
    FETCHING_MAGNET_COMPLETED,
    FETCHING_HTTP_COMPLETED,
    ERROR
}
