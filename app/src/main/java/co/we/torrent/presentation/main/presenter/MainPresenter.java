package co.we.torrent.presentation.main.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.Map;

import javax.inject.Inject;

import co.we.torrent.R;
import co.we.torrent.data.local.SettingsStorage;
import co.we.torrent.di.scopes.Main;
import co.we.torrent.navigation.AppRouter;
import co.we.torrent.navigation.Screens;
import co.we.torrent.presentation.main.view.MainView;

@Main
@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    private final AppRouter appRouter;
    private final SettingsStorage settingsStorage;
    private final Map<Integer, TorrentsPresenter> presenters;
    private static final String PRIVACY_LINK = "";
    public static final int TIME_TO_EXIT = 2000; //2 seconds
    private long lastBackTimeClicked = -1;

    @Inject
    public MainPresenter(AppRouter appRouter, SettingsStorage settingsStorage,
                         Map<Integer, TorrentsPresenter> presenters) {
        this.appRouter = appRouter;
        this.presenters = presenters;
        this.settingsStorage = settingsStorage;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
    }

    public void afterFirstOpenedWithDelay() {
        if (settingsStorage.getMainOpenedFirst()) {
            settingsStorage.setMainOpenedFirst(false);
            getViewState().showTutorial(true, 400);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void exit() {
        long time = System.currentTimeMillis();

        if (time - lastBackTimeClicked < TIME_TO_EXIT) {
            appRouter.exit();
        } else {
            lastBackTimeClicked = time;
            appRouter.showMessage(R.string.back_message);
        }
    }

    public void goToSearch(String query) {
        getViewState().toggleLoading(true, R.string.search_loading);
        appRouter.search(query);
        getViewState().toggleSearchMode(false);
    }

    public void toggleDeleteMode(boolean value) {
        getViewState().toggleDeleteMode(value);
        for (Integer key : presenters.keySet()) {
            presenters.get(key).toggleDeleteMode(value);
        }
    }

    public void toggleSearchMode(boolean value) {
        getViewState().toggleSearchMode(value);
    }

    public void onShareClicked() {
        appRouter.share();
    }

    public void onHelpClicked() {
        getViewState().showTutorial(false, 0);
    }

    public void onSettingsClicked() {
        appRouter.navigateTo(Screens.SETTINGS);
    }

    public void onPrivacyClicked() {
        appRouter.openLink(PRIVACY_LINK);
    }

    public void updateWifiOnly(boolean value) {
        settingsStorage.updateWifiOnly(value);
    }
}
