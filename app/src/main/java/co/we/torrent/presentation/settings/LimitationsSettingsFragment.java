package co.we.torrent.presentation.settings;

import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.SwitchPreferenceCompat;
import android.text.InputFilter;
import android.text.TextUtils;

import com.takisoft.fix.support.v7.preference.EditTextPreference;
import com.takisoft.fix.support.v7.preference.PreferenceFragmentCompat;

import co.we.torrent.R;
import co.we.torrent.data.core.TorrentEngine;
import co.we.torrent.other.InputFilterMinMax;
import timber.log.Timber;

public class LimitationsSettingsFragment extends PreferenceFragmentCompat implements Preference.OnPreferenceChangeListener {
    public static LimitationsSettingsFragment newInstance() {
        LimitationsSettingsFragment fragment = new LimitationsSettingsFragment();
        fragment.setArguments(new Bundle());

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SettingsManager pref = new SettingsManager(getActivity().getApplicationContext());
        InputFilter[] speedFilter = new InputFilter[]{new InputFilterMinMax(0, Integer.MAX_VALUE)};
        InputFilter[] connectionsFilter = new InputFilter[]{
                new InputFilterMinMax(TorrentEngine.Settings.MIN_CONNECTIONS_LIMIT, Integer.MAX_VALUE)
        };
        InputFilter[] uploadsFilter = new InputFilter[]{
                new InputFilterMinMax(TorrentEngine.Settings.MIN_UPLOADS_LIMIT, Integer.MAX_VALUE)
        };
        InputFilter[] queueingFilter = new InputFilter[]{new InputFilterMinMax(1, 1000)};

        String keyMaxDownloadSpeedLimit = getString(R.string.pref_key_max_download_speed);
        EditTextPreference maxDownloadSpeedLimit = (EditTextPreference) findPreference(keyMaxDownloadSpeedLimit);
        maxDownloadSpeedLimit.setDialogMessage(R.string.speed_limit_dialog);
        String value = Integer.toString(pref.getInt(keyMaxDownloadSpeedLimit,
                SettingsManager.Default.maxDownloadSpeedLimit) / 1024);
        maxDownloadSpeedLimit.getEditText().setFilters(speedFilter);
        maxDownloadSpeedLimit.setSummary(value);
        maxDownloadSpeedLimit.setText(value);
        bindOnPreferenceChangeListener(maxDownloadSpeedLimit);

        String keyMaxUploadSpeedLimit = getString(R.string.pref_key_max_upload_speed);
        EditTextPreference maxUploadSpeedLimit = (EditTextPreference) findPreference(keyMaxUploadSpeedLimit);
        maxUploadSpeedLimit.setDialogMessage(R.string.speed_limit_dialog);
        value = Integer.toString(pref.getInt(keyMaxUploadSpeedLimit,
                SettingsManager.Default.maxUploadSpeedLimit) / 1024);
        maxUploadSpeedLimit.getEditText().setFilters(speedFilter);
        maxUploadSpeedLimit.setSummary(value);
        maxUploadSpeedLimit.setText(value);
        bindOnPreferenceChangeListener(maxUploadSpeedLimit);

        String keyMaxConnections = getString(R.string.pref_key_max_connections);
        EditTextPreference maxConnections = (EditTextPreference) findPreference(keyMaxConnections);
        maxConnections.setDialogMessage(R.string.pref_max_connections_summary);
        value = Integer.toString(pref.getInt(keyMaxConnections, SettingsManager.Default.maxConnections));
        maxConnections.getEditText().setFilters(connectionsFilter);
        maxConnections.setSummary(value);
        maxConnections.setText(value);
        bindOnPreferenceChangeListener(maxConnections);

        String keyAutoManage = getString(R.string.pref_key_auto_manage);
        SwitchPreferenceCompat autoManage = (SwitchPreferenceCompat) findPreference(keyAutoManage);
        autoManage.setChecked(pref.getBoolean(keyAutoManage, SettingsManager.Default.autoManage));
        bindOnPreferenceChangeListener(autoManage);

        String keyMaxActiveUploads = getString(R.string.pref_key_max_active_uploads);
        EditTextPreference maxActiveUploads = (EditTextPreference) findPreference(keyMaxActiveUploads);
        value = Integer.toString(pref.getInt(keyMaxActiveUploads, SettingsManager.Default.maxActiveUploads));
        maxActiveUploads.getEditText().setFilters(queueingFilter);
        maxActiveUploads.setSummary(value);
        maxActiveUploads.setText(value);
        bindOnPreferenceChangeListener(maxActiveUploads);

        String keyMaxActiveDownloads = getString(R.string.pref_key_max_active_downloads);
        EditTextPreference maxActiveDownloads = (EditTextPreference) findPreference(keyMaxActiveDownloads);
        value = Integer.toString(pref.getInt(keyMaxActiveDownloads, SettingsManager.Default.maxActiveDownloads));
        maxActiveDownloads.getEditText().setFilters(queueingFilter);
        maxActiveDownloads.setSummary(value);
        maxActiveDownloads.setText(value);
        bindOnPreferenceChangeListener(maxActiveDownloads);

        String keyMaxActiveTorrents = getString(R.string.pref_key_max_active_torrents);
        EditTextPreference maxActiveTorrents = (EditTextPreference) findPreference(keyMaxActiveTorrents);
        value = Integer.toString(pref.getInt(keyMaxActiveTorrents, SettingsManager.Default.maxActiveTorrents));
        maxActiveTorrents.getEditText().setFilters(queueingFilter);
        maxActiveTorrents.setSummary(value);
        maxActiveTorrents.setText(value);
        bindOnPreferenceChangeListener(maxActiveTorrents);

        String keyMaxConnectionsPerTorrent = getString(R.string.pref_key_max_connections_per_torrent);
        EditTextPreference maxConnectionsPerTorrent = (EditTextPreference) findPreference(keyMaxConnectionsPerTorrent);
        maxConnectionsPerTorrent.setDialogMessage(R.string.pref_max_connections_per_torrent_summary);
        value = Integer.toString(pref.getInt(keyMaxConnectionsPerTorrent, SettingsManager.Default.maxConnectionsPerTorrent));
        maxConnectionsPerTorrent.getEditText().setFilters(connectionsFilter);
        maxConnectionsPerTorrent.setSummary(value);
        maxConnectionsPerTorrent.setText(value);
        bindOnPreferenceChangeListener(maxConnectionsPerTorrent);

        String keyMaxUploadsPerTorrent = getString(R.string.pref_key_max_uploads_per_torrent);
        EditTextPreference maxUploadsPerTorrent = (EditTextPreference) findPreference(keyMaxUploadsPerTorrent);
        maxUploadsPerTorrent.setDialogMessage(R.string.pref_max_uploads_per_torrent_summary);
        value = Integer.toString(pref.getInt(keyMaxUploadsPerTorrent, SettingsManager.Default.maxUploadsPerTorrent));
        maxUploadsPerTorrent.getEditText().setFilters(uploadsFilter);
        maxUploadsPerTorrent.setSummary(value);
        maxUploadsPerTorrent.setText(value);
        bindOnPreferenceChangeListener(maxUploadsPerTorrent);
    }

    @Override
    public void onCreatePreferencesFix(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.pref_limitations, rootKey);
    }

    private void bindOnPreferenceChangeListener(Preference preference) {
        preference.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        SettingsManager pref = new SettingsManager(getActivity().getApplicationContext());

        if (preference.getKey().equals(getString(R.string.pref_key_max_connections)) ||
                preference.getKey().equals(getString(R.string.pref_key_max_connections_per_torrent))) {
            int value = TorrentEngine.Settings.MIN_CONNECTIONS_LIMIT;
            if (!TextUtils.isEmpty((String) newValue)) {
                value = getValue(newValue, TorrentEngine.Settings.MIN_CONNECTIONS_LIMIT);
            }
            pref.put(preference.getKey(), value);
            preference.setSummary(Integer.toString(value));
        } else if (preference.getKey().equals(getString(R.string.pref_key_max_uploads_per_torrent))) {
            int value = TorrentEngine.Settings.MIN_UPLOADS_LIMIT;
            if (!TextUtils.isEmpty((String) newValue)) {
                value = getValue(newValue, TorrentEngine.Settings.MIN_UPLOADS_LIMIT);
            }
            pref.put(preference.getKey(), value);
            preference.setSummary(Integer.toString(value));
        } else if (preference.getKey().equals(getString(R.string.pref_key_max_upload_speed)) ||
                preference.getKey().equals(getString(R.string.pref_key_max_download_speed))) {
            int value = 0;
            int summary = 0;
            if (!TextUtils.isEmpty((String) newValue)) {
                summary = getValue(newValue, 0);
                value = summary * 1024;

                if (value < 0) {
                    value = 0;
                    summary = 0;
                }
            }
            pref.put(preference.getKey(), value);
            preference.setSummary(Integer.toString(summary));
        } else if (preference.getKey().equals(getString(R.string.pref_key_max_active_downloads)) ||
                preference.getKey().equals(getString(R.string.pref_key_max_active_uploads)) ||
                preference.getKey().equals(getString(R.string.pref_key_max_active_torrents))) {
            int value = 1;
            if (!TextUtils.isEmpty((String) newValue)) {
                value = getValue(newValue, 1);
            }
            pref.put(preference.getKey(), value);
            preference.setSummary(Integer.toString(value));
        } else if (preference.getKey().equals(getString(R.string.pref_key_auto_manage))) {
            pref.put(preference.getKey(), (boolean) newValue);
        }

        return true;
    }

    private int getValue(Object newValue, int def) {
        int value;
        try {
            value = Integer.parseInt((String) newValue);
        } catch (NumberFormatException e) {
            Timber.e(e);
            value = def;
        }
        return value;
    }
}
