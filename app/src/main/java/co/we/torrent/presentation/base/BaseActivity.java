package co.we.torrent.presentation.base;


import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import co.we.torrent.AndroidApplication;
import co.we.torrent.R;
import co.we.torrent.data.local.SettingsStorage;
import co.we.torrent.navigation.BackButtonListener;
import co.we.torrent.navigation.OpenBrowser;
import co.we.torrent.navigation.Screens;
import co.we.torrent.navigation.Search;
import co.we.torrent.navigation.Share;
import co.we.torrent.navigation.SystemLocalizedMessage;
import co.we.torrent.presentation.activities.MainActivity;
import co.we.torrent.presentation.activities.SettingsActivity;
import co.we.torrent.presentation.main.ui.MainFragment;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportFragmentNavigator;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Forward;
import timber.log.Timber;

/**
 * @author Alexander Artemov
 */
public abstract class BaseActivity extends AppCompatActivity {

    private final static int ANIM_DEFAULT_TIME = 300;
    private static final String TORRENT = "\"torrent\"";
    private static final String MAGNET = "\"magnet\"";
    private static final String PREFIX = MAGNET + " " + TORRENT + " ";
    private static final String GOOGLE_SEARCH = "https://www.google.com/search?q=";
    private static final String GOOGLE_NOW = "com.google.android.googlequicksearchbox";

    @Inject protected NavigatorHolder navigatorHolder;
    @Inject protected SettingsStorage settingsStorage;

    protected Navigator navigator = new SupportFragmentNavigator(this.getSupportFragmentManager(), R.id.container_fragment) {
        @Override
        public void applyCommand(Command command) {
            if (command instanceof Forward) {
                if (((Forward) command).getScreenKey().equals(Screens.MAIN)) {
                    goToMainScreen();
                    return;
                }

                if (((Forward) command).getScreenKey().equals(Screens.SETTINGS)) {
                    goToSettings();
                }
            } else if (command instanceof SystemLocalizedMessage) {
                showSystemMessage(getString(((SystemLocalizedMessage) command).getMessageId()));
            } else if (command instanceof OpenBrowser) {
                goToBrowser(((OpenBrowser) command).getLink(), getString(R.string.open));
            } else if (command instanceof Search) {
                goToSearch(((Search) command).getQuery());
            } else if (command instanceof Share) {
                shareApp();
            } else {
                super.applyCommand(command);
            }
        }

        @Override
        protected void setupFragmentTransactionAnimation(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
            if (command instanceof Forward) {
                String screen = ((Forward) command).getScreenKey();
                if (false) {
                    return;
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Slide slide = new Slide();
                    slide.setDuration(ANIM_DEFAULT_TIME);
                    slide.setSlideEdge(Gravity.END);

                    Slide slideFast = new Slide();
                    slideFast.setDuration(ANIM_DEFAULT_TIME / 2);
                    slideFast.setSlideEdge(Gravity.END);

                    Fade fade = new Fade();
                    fade.setDuration(ANIM_DEFAULT_TIME / 2);

                    currentFragment.setExitTransition(fade);
                    currentFragment.setReenterTransition(null);

                    nextFragment.setEnterTransition(slide);
                    nextFragment.setReturnTransition(slideFast);
                } else {
                    android.support.transition.TransitionSet standardSet = new android.support.transition.TransitionSet();
                    android.support.transition.Fade standardFade = new android.support.transition.Fade();
                    standardFade.setDuration(ANIM_DEFAULT_TIME);
                    standardSet.addTransition(standardFade);

                    currentFragment.setExitTransition(standardSet);
                    nextFragment.setEnterTransition(standardSet);
                }

                fragmentTransaction.setReorderingAllowed(true);
            }
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.MAIN:
                    return MainFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        protected void showSystemMessage(String message) {
            Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void exit() {
            BaseActivity.this.finish();
        }
    };

    private void goToSettings() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    private void shareApp() {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text));
        shareIntent.setType("text/plain");
        startActivity(Intent.createChooser(shareIntent, getString(R.string.share)));
    }

    private void goToSearch(String query) {
        String torrentQuery = PREFIX + query;
        boolean success = goToGoogleNow(torrentQuery);
        if (!success) {
            goToBrowser(GOOGLE_SEARCH + torrentQuery, getString(R.string.search));
        }
    }

    private boolean goToGoogleNow(String query) {
        final Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        intent.setPackage(GOOGLE_NOW);
        intent.putExtra(SearchManager.QUERY, query);
        List<ResolveInfo> resolveList = getPackageManager().queryIntentActivities(intent, 0);

        if (resolveList.size() > 0) {
            startActivity(intent);
            return true;
        } else {
            return false;
        }
    }

    private void goToBrowser(String query, String chooserTitle) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(query));
        startActivity(Intent.createChooser(browserIntent, chooserTitle));
    }

    private void goToMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        AndroidApplication.applicationComponent().inject(this);

        if (savedInstanceState == null) {
            openFirstScreen();
        }
    }

    protected abstract void openFirstScreen();

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_fragment);
        if (fragment != null && fragment instanceof BackButtonListener) {
            ((BackButtonListener) fragment).onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.i("RESULT/ BASE reqCode %s, resCode %s, data %s", requestCode, resultCode, data);

    }
}