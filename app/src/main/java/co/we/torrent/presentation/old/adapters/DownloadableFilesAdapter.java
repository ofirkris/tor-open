package co.we.torrent.presentation.old.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import co.we.torrent.R;
import co.we.torrent.data.core.filetree.BencodeFileTree;
import co.we.torrent.data.core.filetree.FileNode;

/*
 * The adapter for representation of downloadable files in a file tree view.
 */

public class DownloadableFilesAdapter extends BaseFileListAdapter<DownloadableFilesAdapter.ViewHolder, BencodeFileTree> {
    private Context context;
    private ViewHolder.ClickListener clickListener;
    private int rowLayout;

    public DownloadableFilesAdapter(List<BencodeFileTree> files, Context context,
                                    int rowLayout, ViewHolder.ClickListener clickListener) {
        this.context = context;
        this.rowLayout = rowLayout;
        this.clickListener = clickListener;
        Collections.sort(files);
        this.files = files;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);

        return new ViewHolder(v, clickListener, files);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final BencodeFileTree file = files.get(position);

        holder.fileName.setText(file.getName());

        if (file.getType() == FileNode.Type.DIR) {
            holder.fileIcon.setImageResource(R.drawable.ic_folder);

        } else if (file.getType() == FileNode.Type.FILE) {
            holder.fileIcon.setImageResource(R.drawable.ic_file);
        }

        if (file.getName().equals(BencodeFileTree.PARENT_DIR)) {
            holder.fileSelected.setVisibility(View.GONE);
            holder.fileSize.setVisibility(View.GONE);
        } else {
            holder.fileSelected.setVisibility(View.VISIBLE);
            holder.fileSelected.setChecked(file.isSelected());
            holder.fileSize.setVisibility(View.VISIBLE);
            holder.fileSize.setText(Formatter.formatFileSize(context, file.size()));
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ClickListener listener;
        private List<BencodeFileTree> files;
        TextView fileName;
        TextView fileSize;
        ImageView fileIcon;
        CheckBox fileSelected;

        public ViewHolder(View itemView, final ClickListener listener, final List<BencodeFileTree> files) {
            super(itemView);

            this.listener = listener;
            this.files = files;
            itemView.setOnClickListener(this);

            fileName = (TextView) itemView.findViewById(R.id.file_name);
            fileSize = (TextView) itemView.findViewById(R.id.file_size);
            fileIcon = (ImageView) itemView.findViewById(R.id.file_icon);
            fileSelected = (CheckBox) itemView.findViewById(R.id.file_selected);
            fileSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onItemCheckedChanged(ViewHolder.this.files.get(getAdapterPosition()),
                                fileSelected.isChecked());
                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            if (listener != null && position >= 0) {
                BencodeFileTree file = files.get(position);

                if (file.getType() == FileNode.Type.FILE) {
                    /* Check file if it clicked */
                    fileSelected.performClick();
                }

                listener.onItemClicked(file);
            }
        }

        public interface ClickListener {
            void onItemClicked(BencodeFileTree node);

            void onItemCheckedChanged(BencodeFileTree node, boolean selected);
        }
    }
}
