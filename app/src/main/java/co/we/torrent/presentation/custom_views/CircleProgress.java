package co.we.torrent.presentation.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import co.we.torrent.R;


/**
 * Created 28.11.2017 by
 *
 * @author Alexander Artemov
 */
public class CircleProgress extends View {

    public static final int FULL = 360;
    private static final int START_ANGLE_POINT = -90;

    private Map<Integer, Paint> paints = new HashMap<>();
    private RectF rect;

    private float angle = 0;

    private float strokeWidth;
    private int layout_width;
    private int layout_height;

    public CircleProgress(Context context, AttributeSet attrs) {
        super(context, attrs);

        int[] attrsArray = new int[]{
                android.R.attr.layout_width, // 0
                android.R.attr.layout_height // 1
        };

        TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
        layout_width = ta.getDimensionPixelSize(0, ViewGroup.LayoutParams.MATCH_PARENT);
        layout_height = ta.getDimensionPixelSize(1, ViewGroup.LayoutParams.MATCH_PARENT);
        ta.recycle();

        strokeWidth = (int) getResources().getDimension(R.dimen.circle_width);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(getRect(), 0, FULL, false, getPaint(R.color.gray));
        canvas.drawArc(getRect(), START_ANGLE_POINT, angle, false, getPaint(R.color.colorPrimary));
    }

    private Paint getPaint(int colorRes) {
        if (!paints.containsKey(colorRes)) {
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(strokeWidth);
            //Circle color
            paint.setColor(getResources().getColor(colorRes));
            paints.put(colorRes, paint);
        }
        return paints.get(colorRes);
    }

    private RectF getRect() {
        if (rect == null) {
            rect = new RectF(strokeWidth, strokeWidth, layout_width - strokeWidth, layout_height - strokeWidth);
        }

        return rect;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public void setProgress(int progress) {
        float angle = ((float) progress / 100) * FULL;
        this.setAngle(angle);
        this.invalidate();
    }
}
