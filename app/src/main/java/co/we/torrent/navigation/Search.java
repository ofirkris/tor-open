package co.we.torrent.navigation;

import ru.terrakok.cicerone.commands.Command;

/**
 * Created 09.01.2018 by
 *
 * @author Alexander Artemov
 */
public class Search implements Command {

    private final String query;

    Search(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }
}
