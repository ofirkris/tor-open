package co.we.torrent.navigation;

/**
 * @author Alexander Artemov
 */
public class Screens {
    public static final String MAIN = "main";
    public static final String SETTINGS = "settings";
}
