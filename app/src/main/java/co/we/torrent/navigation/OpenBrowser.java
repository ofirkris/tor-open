package co.we.torrent.navigation;

import ru.terrakok.cicerone.commands.Command;

/**
 * Created 18.01.18 by
 *
 * @author Alexander Artemov
 */
public class OpenBrowser implements Command {
    private final String link;

    public OpenBrowser(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }
}
