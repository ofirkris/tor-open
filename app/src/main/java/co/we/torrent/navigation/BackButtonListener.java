package co.we.torrent.navigation;

/**
 * @author Alexander Artemov
 */
public interface BackButtonListener {
    void onBackPressed();
}