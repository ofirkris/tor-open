package co.we.torrent.navigation;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.terrakok.cicerone.Router;

/**
 * @author Alexander Artemov
 */
@Singleton
public class AppRouter extends Router {


    @Inject
    public AppRouter() {
    }

    public void showMessage(int messageId) {
        executeCommand(new SystemLocalizedMessage(messageId));
    }

    public void share() {
        executeCommand(new Share());
    }

    public void search(String query) {
        executeCommand(new Search(query));
    }

    public void openLink(String link) {
        executeCommand(new OpenBrowser(link));
    }
}
