package com.braunster.tutorialview.object;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;

public class FontCache {

    private static HashMap<String, Typeface> fontCache = new HashMap<>();
    public static final String REGULAR_FONT =  "fonts/MavenPro-Regular.ttf";
    public static final String MAVEN_BOLD_FONT = "fonts/MavenPro-Bold.ttf";
    public static final String MAVEN_LIGHT_FONT = "fonts/MavenProLight-200.otf";
    public static Typeface getTypeface(String fontname, Context context) {
        Typeface typeface = fontCache.get(fontname);

        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(context.getAssets(), fontname);
            } catch (Exception e) {
                return null;
            }

            fontCache.put(fontname, typeface);
        }

        return typeface;
    }
}