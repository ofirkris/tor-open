package com.braunster.tutorialview.view;

/**
 * Created by Mickael on 8/13/2017.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import com.braunster.tutorialview.R;


/**
 * Created by Mickael on 2/19/2017.
 */

public class RoundedButton extends AppCompatButton {
    public RoundedButton(Context context) {
        super(context);
        init(null);
    }

    public RoundedButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public RoundedButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attrs) {
        if(attrs != null)
        {
            TypedArray typedArray = getContext().obtainStyledAttributes(attrs,
                    R.styleable.RoundedButtonAttrs);

            int count = typedArray.getIndexCount();
            try{
                for (int i = 0; i < count; ++i) {

                    int attr = typedArray.getIndex(i);
                    if(attr == R.styleable.RoundedButtonAttrs_rounded_button_color) {
                        int color = typedArray.getColor(attr, ContextCompat.getColor(getContext(), R.color.colorAccent));
                        setRoundedButtonColor(color);
                    }
                }
            }
            finally {
                typedArray.recycle();
            }
        }
    }

    public void setRoundedButtonColor(int color)
    {
        GradientDrawable bgShape = (GradientDrawable ) getBackground();
        bgShape.setColor(color);
    }


}
