package com.braunster.tutorialview.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.braunster.tutorialview.R;
import com.braunster.tutorialview.object.FontCache;

public class FontTextView extends android.support.v7.widget.AppCompatTextView {

    public FontTextView(Context context) {
        super(context);

        setTypeface(FontCache.getTypeface(FontCache.REGULAR_FONT, context));
    }

    public FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        extractAttributes(context, attrs);
    }

    public FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        extractAttributes(context, attrs);
    }

    private void extractAttributes(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray typedArray = context.getTheme()
                .obtainStyledAttributes(attrs, R.styleable.FontViewStyle, 0, 0);

        int font = typedArray.getInt(R.styleable.FontViewStyle_font_type, 0);
        setTypeface(font, context);

        typedArray.recycle();
    }

    private void setTypeface(int font, Context context) {
        switch (font) {
            default:
            case 0:
                setTypeface(FontCache.getTypeface(FontCache.REGULAR_FONT, context));
                break;
            case 1:
                setTypeface(FontCache.getTypeface(FontCache.MAVEN_BOLD_FONT, context));
                break;
            case 2:
                setTypeface(FontCache.getTypeface(FontCache.MAVEN_LIGHT_FONT, context));
                break;
        }
    }
}
